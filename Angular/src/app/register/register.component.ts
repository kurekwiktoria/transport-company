import {Component, Input, OnInit} from '@angular/core';
import {User} from "../core/dto/User";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {UserService} from "../services/user/user.service";
import {userError} from "@angular/compiler-cli/src/transformers/util";
import {Company} from "../core/dto/Company";
import {Form, FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService) { }

  emailForm = new FormControl('', [Validators.required, Validators.email]);

  nameForm = new FormControl('', Validators.required);

  userForm : FormGroup;

  companyForm : FormGroup;

  ngOnInit() {
    this.userForm = new FormGroup({
      pesel: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      login: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      surname: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      type: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      idNumber: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    });


    this.companyForm = new FormGroup({
      nip: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      city: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      street: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      phone: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      fax: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.email
      ]),
      regon: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      krs: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      zipCode: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

    saveUserClick() {
      let user : User = this.userForm.value;
      if (this.userForm.valid) {
        console.log("Wysłano" + user);
        this.userService.addUser(user);
      }
      else {
        console.log("Walidacja" + user.name);
      }
    }

    saveForwarderClick() {
      let user : User = this.userForm.value;
      user.firm = this.companyForm.value;
      if (this.userForm.valid && this.companyForm.valid) {
        this.userService.addForwarder(user);
        console.log("Wysłano forwardera" + user.toString());
      }
    }

    getErrorMessage() {
      return this.emailForm.hasError('required') ? 'To pole jest wymagane, wprowadź wartość' :
        this.emailForm.hasError('email') ? 'E-mail nie jest poprawny' :
          '';
    }

  }
