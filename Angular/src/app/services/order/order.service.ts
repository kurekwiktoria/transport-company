import { Injectable } from '@angular/core';
import {Order} from "../../core/dto/Order";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private addOrderURL = "http://localhost:8080/api/addOrder";

  private getAllOrdersURL = "http://localhost:8080/api/getAllOrders";

  private getAllNotAcceptedOrdersURL = "http://localhost:8080/api/getAllNotAcceptedOrders";

  private getAllOrdersByUserPeselURL = "http://localhost:8080/api/getAllOrdersByUserPesel";

  private getAllAvailableOrdersURL = "http://localhost:8080/api/getAllAvailableOrders";

  private getOrderByIdURL = "http://localhost:8080/api/getOrderById";

  private getLastOrderURL = "http://localhost:8080/api/getLastOrder"

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addOrder(order: Order): Promise<Order> {
    return this.http.post<Order>(
      this.addOrderURL,
      order,
      { headers: this.headers}
      ).toPromise().then(res => res as Order).catch(this.handleError);
  }

  getAllOrders(){
    return this.http.get<Order[]>(this.getAllOrdersURL);
  }


  getAllNotAcceptedOrders(){
    return this.http.get<Order[]>(this.getAllNotAcceptedOrdersURL);
  }

  getAllOrdersByUserPesel(pesel : string){
    return this.http.get<Order[]>(this.getAllOrdersByUserPeselURL + '/' + pesel);
  }

  getAllAvailableOrders(){
    return this.http.get<Order[]>(this.getAllAvailableOrdersURL);
  }

  getOrderById(id : string){
    return this.http.get<Order>(this.getOrderByIdURL + '/' + id);
  }

  getLastOrder(){
    return this.http.get<Order>(this.getLastOrderURL);
  }

}

