import { Injectable } from '@angular/core';
import {Vehicle} from "../../core/dto/Vehicle";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  private addVehicleURL = "http://localhost:8080/api/addVehicle";

  private getAllVehiclesURL = "http://localhost:8080/api/getAllVehicles";

  private getAllVehiclesByFirmNipURL = "http://localhost:8080/api/getAllVehiclesByFirmNip";

  private deleteVehicleByVinURL = "http://localhost:8080/api/deleteVehicle"

  private getVehicleByVinURL = "http://localhost:8080/api/getVehicleByVin"

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  // addVehicle(vehicle: Vehicle): Promise<Vehicle> {
  //   return this.http.post<Vehicle>(
  //     this.addVehicleURL,
  //     vehicle,
  //     { headers: this.headers}
  //     ).toPromise().then(res => res as Vehicle).catch(this.handleError);
  // }

  addVehicle(vehicle: Vehicle) {
    return this.http.post(this.addVehicleURL, vehicle);
  }

  getAllVehicles() {
    return this.http.get<Vehicle[]>(this.getAllVehiclesURL);
  }

  getAllVehiclesByFirmNip(nip : string) {
    return this.http.get<Vehicle[]>(this.getAllVehiclesByFirmNipURL + '/' + nip);
  }

  getVehicleByVin(vin : string){
    return this.http.get<Vehicle>(this.getVehicleByVinURL + '/' + vin);
  }

  delete(vin : string) {
    return this.http.delete(this.deleteVehicleByVinURL + '/' + vin);
  }
}

