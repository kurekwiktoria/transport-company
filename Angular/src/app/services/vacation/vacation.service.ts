import { Injectable } from '@angular/core';
import {Vacation} from "../../core/dto/Vacation";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class VacationService {

  private addVacationURL = "http://localhost:8080/api/addVacation";

  private getVacationsByDriverFirmNipURL = "http://localhost:8080/api/getVacationsByDriverFirmNip";

  private getLastVacationURL = "http://localhost:8080/api/getLastVacation";

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addVacation(vacation: Vacation): Promise<Vacation> {
    return this.http.post<Vacation>(
      this.addVacationURL,
      vacation,
      { headers: this.headers}
      ).toPromise().then(res => res as Vacation).catch(this.handleError);
  }

  getVacationsByDriverFirmNip(nip : string) {
    return this.http.get<Vacation[]>(this.getVacationsByDriverFirmNipURL + '/' + nip);
  }

  getLastVacation() {
    return this.http.get<Vacation>(this.getLastVacationURL);
  }
}

