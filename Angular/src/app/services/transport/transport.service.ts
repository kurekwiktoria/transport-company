import { Injectable } from '@angular/core';
import {Transport} from "../../core/dto/Transport";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TransportService {

  private addTransportURL = "http://localhost:8080/api/addTransport";

  private getAllTransportsByFirmNipURL = "http://localhost:8080/api/getAllTransportsByFirmNip";

  private getAllTransportsByDriverPeselURL = "http://localhost:8080/api/getAllTransportsByDriverPesel";

  private getLastTransportURL = "http://localhost:8080/api/getLastTransport";

  private getFutureTransportsByDriverPeselURL = "http://localhost:8080/api/getFutureTransportsByDriverPesel";

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addTransport(transport: Transport): Promise<Transport> {
    return this.http.post<Transport>(
      this.addTransportURL,
      transport,
      { headers: this.headers}
      ).toPromise().then(res => res as Transport).catch(this.handleError);
  }

  saveTransport(transport: Transport) {
    return this.http.post(this.addTransportURL, transport);
  }

  getAllTransportsByFirmNip(nip : string) {
    return this.http.get<Transport[]>(this.getAllTransportsByFirmNipURL + '/' + nip);
  }

  getAllTransportsByDriverPesel(pesel : string) {
    return this.http.get<Transport[]>(this.getAllTransportsByDriverPeselURL + '/' + pesel);
  }

  getLastTransport() {
    return this.http.get<Transport>(this.getLastTransportURL);
  }

  getFutureTransportsByDriverPesel(pesel : string) {
    return this.http.get<Transport[]>(this.getFutureTransportsByDriverPeselURL + '/' + pesel);
  }
}

