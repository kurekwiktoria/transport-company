import { Injectable } from '@angular/core';
import {User} from "../../core/dto/User";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Company} from "../../core/dto/Company";
import {Driver} from "../../core/dto/Driver";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private addUserURL = "http://localhost:8080/api/addClient";

  private addForwarderURL = "http://localhost:8080/api/addForwarder";

  private addDriverURL = "http://localhost:8080/api/addDriver";

  private addClientURL = "http://localhost:8080/api/addClient";

  private getAllURL = "http://localhost:8080/api/getAll";

  private getAllCountURL = "http://localhost:8080/api/getAllCount";

  private deleteURL = "http://localhost:8080/api/delete";

  private getAllDriversURL = "http://localhost:8080/api/getAllDrivers";

  private getActiveDriversURL = "http://localhost:8080/api/getAllDriversByState/ACTIVE";

  private getAllDriversWithoutFirmURL = "http://localhost:8080/api/getAllDriversWithoutFirm";

  private getAllDriversOnVacationURL = "http://localhost:8080/api/getAllDriversOnVacation";

  private getAllDriversByFirmURL = "http://localhost:8080/api/getAllDriversByFirm";

  private getDriverByPeselURL = "http://localhost:8080/api/getDriverByPesel";

  private getAllDriversWithExpiredMedicalExaminationURL = "http://localhost:8080/api/getAllDriversWithExpiredMedicalExamination";

  private getAllDriversWithExpiredDrivingLicenceURL = "http://localhost:8080/api/getAllDriversWithExpiredDrivingLicence";

  private makeDriverFiredURL = "http://localhost:8080/api/makeDriverFired";

  private makeDriverHiredURL = "http://localhost:8080/api/makeDriverHired";

  private updateUserURL = "http://localhost:8080/api/updateUser";

  private updateDrivingLicenceURL = "http://localhost:8080/api/updateDrivingLicence";

  private updateMedicalExaminationURL = "http://localhost:8080/api/updateMedicalExamination";

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addUser(user: User): Promise<User> {
    return this.http.post<User>(
      this.addUserURL,
      user,
      { headers: this.headers}
    ).toPromise().then(res => res as User).catch(this.handleError);
  }

  addForwarder(user: User): Promise<User> {
    return this.http.post<User>(
      this.addForwarderURL,
      user,
      { headers: this.headers}
    ).toPromise().then(res => res as User).catch(this.handleError);
  }

  saveUser(user: User) {
    return this.http.post(this.addUserURL, user);
  }

  saveForwarder(user: User) {
    return this.http.post(this.addForwarderURL, user);
  }

  addDriver(user: User): Promise<User> {
    return this.http.post<User>(
      this.addDriverURL,
      user,
      { headers: this.headers}
    ).toPromise().then(res => res as User).catch(this.handleError);
  }

  addClient(user: User): Promise<User> {
    return this.http.post<User>(
      this.addClientURL,
      user,
      { headers: this.headers}
    ).toPromise().then(res => res as User).catch(this.handleError);
  }

  // getAll() {
  //   return this.http.get<User[]>(this.getAllURL);
  // }

  getAll(pageNo : number, pageSize : number) {
    return this.http.get<User[]>(this.getAllURL + '/?pageNo=' + pageNo + '&pageSize=' + pageSize);
  }

  getAllCount() {
    return this.http.get<Number>(this.getAllCountURL);
  }

  getAllDrivers() {
    return this.http.get<User[]>(this.getAllDriversURL);
  }

  getActiveDrivers() {
    return this.http.get<User[]>(this.getActiveDriversURL);
  }

  delete(pesel : string) {
    return this.http.delete(this.deleteURL + '/' + pesel);
  }

  getAllDriversWithoutFirm() {
    return this.http.get<User[]>(this.getAllDriversWithoutFirmURL);
  }


  getAllDriversOnVacation() {
    return this.http.get<User[]>(this.getAllDriversOnVacationURL);
  }

  getAllDriversByFirm(nip : string){
    return this.http.get<Driver[]>(this.getAllDriversByFirmURL + '/' + nip);
  }

  getDriverByPesel(pesel : string){
    return this.http.get<Driver>(this.getDriverByPeselURL + '/' + pesel);
  }

  getAllDriversWithExpiredMedicalExamination(){
    return this.http.get<User[]>(this.getAllDriversWithExpiredMedicalExaminationURL);
  }

  getAllDriversWithExpiredDrivingLicence(){
    return this.http.get<User[]>(this.getAllDriversWithExpiredDrivingLicenceURL);
  }

  makeDriverFired(pesel : string){
    return this.http.get<User[]>(this.makeDriverFiredURL + '/' + pesel);
  }

  makeDriverHired(pesel : string, nip : string){
    return this.http.get<User[]>(this.makeDriverHiredURL + '/' + pesel + '/' + nip);
  }

  updateUser(pesel : string, user : User){
    return this.http.get<User>(this.updateUserURL);
  }

  updateDrivingLicence(date : string, pesel : string){
    const fd = new FormData();
    fd.append('date', date);
    fd.append('pesel', pesel);
    return this.http.post(this.updateDrivingLicenceURL, fd, {
      reportProgress: true,
      observe: 'events'
    });
  }

  updateMedicalExamination(date : string, pesel : string){
    const fd = new FormData();
    fd.append('date', date);
    fd.append('pesel', pesel);
    return this.http.post(this.updateMedicalExaminationURL, fd, {
      reportProgress: true,
      observe: 'events'
    });
  }

  saveDriver(driver: Driver) {
    return this.http.post(this.addDriverURL, driver);
  }
}

