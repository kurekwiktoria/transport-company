import { Injectable } from '@angular/core';
import {Order} from "../../core/dto/Order";

@Injectable({
  providedIn: 'root'
})
export class PassingService {


  private _order: Order;

  constructor() { }


  get order(): Order {
    const order = this._order;
    this._order = null;
    return order;
  }

  set order(value: Order) {
    this._order = value;
  }
}
