import { TestBed } from '@angular/core/testing';

import { PassingService } from './passing.service';

describe('PassingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PassingService = TestBed.get(PassingService);
    expect(service).toBeTruthy();
  });
});
