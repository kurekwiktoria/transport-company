import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Truck} from "../../core/dto/Truck";
import {User} from "../../core/dto/User";

@Injectable({
  providedIn: 'root'
})
export class TruckService {

  truckURL = "http://localhost:8080/api/getAllVehicles";

  constructor(private http: HttpClient) { }

  getTrucks() {
    return this.http.get<Truck[]>(this.truckURL);
  }
}
