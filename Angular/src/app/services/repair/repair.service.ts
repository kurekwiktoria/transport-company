import { Injectable } from '@angular/core';
import {Transport} from "../../core/dto/Transport";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Repair} from "../../core/dto/Repair";

@Injectable({
  providedIn: 'root'
})
export class RepairService {

  private addRepairURL = "http://localhost:8080/api/addRepair";

  private getAllRepairsURL = "http://localhost:8080/api/getAllRepairs";

  private getLastRepairURL = "http://localhost:8080/api/getLastRepair";

  private getRepairsByVehicleVinURL = "http://localhost:8080/api/getRepairsByVehicleVin";

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addRepair(repair: Repair): Promise<Repair> {
    return this.http.post<Repair>(
      this.addRepairURL,
      repair,
      { headers: this.headers}
      ).toPromise().then(res => res as Repair).catch(this.handleError);
  }

  getAllRepairs() {
    return this.http.get<Repair[]>(this.getAllRepairsURL);
  }

  getRepairsByVehicleVin(vin : string) {
    return this.http.get<Repair[]>(this.getRepairsByVehicleVinURL + '/' + vin);
  }

  getLastRepair() {
    return this.http.get<Repair>(this.getLastRepairURL);
  }
}

