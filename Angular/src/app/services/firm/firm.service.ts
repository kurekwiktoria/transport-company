import { Injectable } from '@angular/core';
import {Vehicle} from "../../core/dto/Vehicle";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Firm} from "../../core/dto/Firm";
import {Company} from "../../core/dto/Company";

@Injectable({
  providedIn: 'root'
})
export class FirmService {

  private addFirmURL = "http://localhost:8080/api/addVehicle";

  private getAllFirmsURL = "http://localhost:8080/api/getAllFirms";

  private getFirmEntityByNameURL = "http://localhost:8080/api/getFirmEntityByName";

  private getFirmEntityByNipURL = "http://localhost:8080/api/getFirmEntityByNip";

  private updateFirmURL = "http://localhost:8080/api/updateFirm";

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {

  }

  private handleError(err: any): Promise<any> {
    console.log(err);
    return null;
  }

  addFirm(firm: Firm): Promise<Firm> {
    return this.http.post<Firm>(
      this.addFirmURL,
      firm,
      { headers: this.headers}
      ).toPromise().then(res => res as Firm).catch(this.handleError);
  }

  getFirmEntityByName(name : string) {
    return this.http.get<Firm>(this.getFirmEntityByNameURL + '/' + name);
  }

  getFirmEntityByNip(nip : string) {
    return this.http.get<Firm>(this.getFirmEntityByNipURL + '/' + nip);
  }

  updateFirm(firm : Firm) {
    return this.http.put(this.updateFirmURL, firm);
  }

}

