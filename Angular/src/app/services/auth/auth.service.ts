import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../core/dto/User";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  private getUserByLoginURL = "http://localhost:8080/api/getUserByLogin";

  user: User;

  logIn(login: string) {
    return this.http.get(this.getUserByLoginURL + '/' + login);
  }

  logout(): void {
    this.user = null;
  }


}
