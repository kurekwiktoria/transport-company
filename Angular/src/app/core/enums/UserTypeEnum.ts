export enum UserTypeEnum {
  DRIVER = 'DRIVER',
  FORWARDER = 'FORWARDER',
  ADMIN = 'ADMIN',
  CLIENT = 'CLIENT',
}
