import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../../services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    let userType;

    if (this.authService.user != null) {
      userType = this.authService.user.type;
      if ( url.includes('driver') && userType == 'DRIVER')
        return true;
      else if ( url.includes('shipper') && userType == 'FORWARDER')
        return true;
      else if ( url.includes('customer') && userType == 'CLIENT')
        return true;
      else if ( userType == 'ADMIN')
        return true;
    }


    if (userType == null)
      this.router.navigate(['/login']);

    return false;
  }
}
