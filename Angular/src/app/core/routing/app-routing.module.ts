import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserComponent} from "../../user/user.component";
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "../../dashboard/dashboard.component";
import {ShipperComponent} from "../../shipper/shipper.component";
import {DriverComponent} from "../../driver/driver.component";
import {LoginComponent} from "../../login/login.component";
import {CustomerComponent} from "../../customer/customer.component";
import {AddUserDialogComponent} from "../../user/add-user-dialog/add-user-dialog.component";
import {RegisterComponent} from "../../register/register.component";
import {ShowDriversComponent} from "../../shipper/show-drivers/show-drivers.component";
import {ShowTransportsComponent} from "../../shipper/show-transports/show-transports.component";
import {AvailableOrdersComponent} from "../../shipper/available-orders/available-orders.component";
import {ShowTrucksComponent} from "../../shipper/show-trucks/show-trucks.component";
import {ShowVacationsComponent} from "../../shipper/show-vacations/show-vacations.component";
import {AddDriversComponent} from "../../shipper/add-drivers/add-drivers.component";
import {EditProfileComponent} from "../../shipper/edit-profile/edit-profile.component";
import {EditCompanyComponent} from "../../shipper/edit-company/edit-company.component";
import {ReportFailureComponent} from "../../driver/report-failure/report-failure.component";
import {ShowDetailsComponent} from "../../shipper/show-drivers/show-details/show-details.component";
import {VacationRequestComponent} from "../../driver/vacation-request/vacation-request.component";
import {EditDriverComponent} from "../../driver/edit-driver/edit-driver.component";
import {EditCustomerComponent} from "../../customer/edit-customer/edit-customer.component";
import {AddOrderComponent} from "../../customer/add-order/add-order.component";
import {ShowOrdersComponent} from "../../customer/show-orders/show-orders.component";
import {ShowTransportHistoryComponent} from "../../driver/show-transport-history/show-transport-history.component";
import {ShowStatisticsComponent} from "../../driver/show-statistics/show-statistics.component";
import {AddTruckComponent} from "../../shipper/show-trucks/add-truck/add-truck.component";
import {AcceptOrderComponent} from "../../shipper/available-orders/accept-order/accept-order.component";
import {FinishOrderComponent} from "../../driver/show-transport-history/finish-order/finish-order.component";
import {AuthGuard} from "../auth/auth.guard";

const routes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'driver',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DriverComponent},
      { path: 'show-transport-history', component: ShowTransportHistoryComponent},
      { path: 'report-failure', component: ReportFailureComponent},
      { path: 'vacation-request', component: VacationRequestComponent},
      { path: 'edit-driver', component: EditDriverComponent},
      { path: 'show-statistics', component: ShowStatisticsComponent},
      { path: 'show-transport-history/finish-order', component: FinishOrderComponent},
    ]
  },
  { path: 'login', component: LoginComponent},
  { path: 'customer',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: CustomerComponent},
      { path: 'edit-customer', component: EditCustomerComponent},
      { path: 'add-order', component: AddOrderComponent},
      { path: 'show-orders', component: ShowOrdersComponent},
    ]
  },
  { path: 'addUser', component: AddUserDialogComponent},
  { path: 'register', component: RegisterComponent},
  { path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  { path: 'shipper',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: ShipperComponent},
      { path: 'show-drivers', component: ShowDriversComponent},
      { path: 'show-transports', component: ShowTransportsComponent},
      { path: 'available-orders', component: AvailableOrdersComponent},
      { path: 'available-orders/accept-order', component: AcceptOrderComponent},
      { path: 'show-trucks/add-truck', component: AddTruckComponent},
      { path: 'show-trucks', component: ShowTrucksComponent},
      { path: 'show-vacations', component: ShowVacationsComponent},
      { path: 'add-drivers', component: AddDriversComponent},
      { path: 'edit-profile', component: EditProfileComponent},
      { path: 'edit-company', component: EditCompanyComponent},
      { path: 'show-drivers/show-details', component: ShowDetailsComponent},
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule]
})

export class AppRoutingModule { }
