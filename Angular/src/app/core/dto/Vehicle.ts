import {Company} from "./Company";

export class Vehicle {

  public vin;

  public brand;

  public model;

  public power;

  public tankCapacity;

  public combustion;

  public vehicleCapacity;

  public serviceCheckExpiryDate;

  public insuranceExpiryDate;

  public state;

  public stateDescr;

  public mileage;

  public type;

  public typeDescr;

  public registrationNumber;

  public firm: Company;

  constructor(vin, brand, model, power, tankCapacity, combustion, vehicleCapacity, serviceCheckExpiryDate, insuranceExpiryDate,
              state, mileage, type, registrationNumber) {

    this.vin = vin;
    this.brand = brand;
    this.model = model;
    this.power = power;
    this.tankCapacity = tankCapacity;
    this.combustion = combustion;
    this.vehicleCapacity = vehicleCapacity;
    this.serviceCheckExpiryDate = serviceCheckExpiryDate;
    this.insuranceExpiryDate = insuranceExpiryDate;
    this.state = state;
    this.mileage = mileage;
    this.type = type;
    this.registrationNumber = registrationNumber;
  }

  static getEmptyVehicle() : Vehicle {
    return new Vehicle(null, null,null,
      null, null, null, null,
      null, null,null,null,
      null, null);
  }

  public validate() : boolean{
        return this.vin != null && this.brand != null && this.model != null && this.power != null &&
          this.tankCapacity != null && this.combustion != null && this.vehicleCapacity != null &&
          this.serviceCheckExpiryDate != null && this.insuranceExpiryDate != null && this.state != null &&
          this.mileage != null && this.type != null && this.registrationNumber != null;
  }

  public toString(): string {

    return this.vin + " " + this.brand + " " + this.model + " " + this.power + " " +
      this.tankCapacity + " " + this.combustion + " " + this.vehicleCapacity + " " +
      this.serviceCheckExpiryDate + " " + this.insuranceExpiryDate + " " + this.state + " " +
      this.mileage + " " + this.type + " " + this.registrationNumber + " " + this.firm.nip;
  }
}
