export class Truck {

  public vin;

  public brand;

  public model;

  public power;

  public tankCapacity;

  public combustion;

  public vehicleCapacity;

  public serviceCheckExpiryDate;

  public insuranceExpiryDate;

  public state;

  public mileage;

  public type;

  public registrationNumber;

  public transports;

  constructor(vin, brand, model, power, tankCapacity, combustion, vehicleCapacity, serviceCheckExpiryDate, insuranceExpiryDate, state, mileage, type, registrationNumber, transports) {
    this.vin = vin;
    this.brand = brand;
    this.model = model;
    this.power = power;
    this.tankCapacity = tankCapacity;
    this.combustion = combustion;
    this.vehicleCapacity = vehicleCapacity;
    this.serviceCheckExpiryDate = serviceCheckExpiryDate;
    this.insuranceExpiryDate = insuranceExpiryDate;
    this.state = state;
    this.mileage = mileage;
    this.type = type;
    this.registrationNumber = registrationNumber;
    this.transports = transports;
  }
}
