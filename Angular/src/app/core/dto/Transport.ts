import {Company} from "./Company";
import {Order} from "./Order";
import {Vehicle} from "./Vehicle";
import {Driver} from "./Driver";

export class Transport {

  public id;

  public burnedFuel;

  public driverSalary;

  public profit;

  public isFinished;

  public order : Order;

  public vehicle : Vehicle;

  public driver : Driver;


  constructor(id, burnedFuel, driverSalary, profit, isFinished) {

    this.id = id;
    this.burnedFuel = burnedFuel;
    this.driverSalary = driverSalary;
    this.profit = profit;
    this.isFinished = isFinished;
  }

  static getEmptyTransport() : Transport {
    return new Transport(null, null,null,
      null, null);
  }

  public validate() : boolean{
        return this.id != null && this.burnedFuel != null && this.driverSalary != null &&
          this.profit != null && this.isFinished != null;
  }

  public toString(): string {

    return this.id + " " +  this.burnedFuel + " " + this.driverSalary + " " +
      this.profit + " " + this.isFinished;
  }
}
