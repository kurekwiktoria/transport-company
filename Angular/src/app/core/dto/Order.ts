import {User} from "./User";
import DateTimeFormat = Intl.DateTimeFormat;

export class Order {

  public id;

  public loadingPlace;

  public destination;

  public loadingDate : Date;

  public unloadingDate : Date;

  public isFinished;

  public distance;

  public price;

  public description;

  public user: User;

  constructor(id, loadingPlace, destination, loadingDate, unloadingDate, isFinished, distance, price, description) {

    this.id = id;
    this.loadingPlace = loadingPlace;
    this.destination = destination;
    this.loadingDate = loadingDate;
    this.unloadingDate = unloadingDate;
    this.isFinished = isFinished;
    this.distance = distance;
    this.price = price;
    this.description = description;
  }

  static getEmptyOrder() : Order {
    return new Order(null, null,null,
      null, null, null, null,
      null, null);
  }

  public validate() : boolean {
    return this.id != null && this.loadingPlace != null && this.destination != null && this.loadingDate != null &&
      this.unloadingDate != null && this.isFinished != null && this.distance != null && this.price != null &&
      this.description != null;
  }

  public toString(): string {
    return this.id+ " " +this.loadingPlace+ " " +this.destination+ " " +this.loadingDate+ " " +
      this.unloadingDate+ " " +this.isFinished+ " " +this.distance+ " " +this.price+ " " +
      this.description+ " " + this.user.pesel;
  }
}
