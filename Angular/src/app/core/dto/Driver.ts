import {User} from "./User";

export class Driver extends User {


  public drivingLicenceExpiryDate;

  public medicalExaminationExpiryDate;

  public state;

  public stateDescr;
}
