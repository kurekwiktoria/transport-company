import {Company} from "./Company";
import {Order} from "./Order";
import {Vehicle} from "./Vehicle";
import {Driver} from "./Driver";

export class Repair {

  public id;

  public vehicle : Vehicle;

  public cost;

  public description;

  public dateFrom;

  public dateTo;


  constructor(id, cost, description, dateFrom, dateTo, vehicle) {

    this.id = id;
    this.cost = cost;
    this.description = description;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.vehicle = vehicle;
  }

  static getEmptyRepair() : Repair {
    return new Repair(null, null,null,
      null, null, null);
  }

  public validate() : boolean{
        return this.id != null && this.cost != null && this.description != null &&
          this.dateFrom != null && this.dateTo != null && this.vehicle != null;
  }

  public toString(): string {

    return this.id + " " +  this.description + " " + this.cost + " " +
      this.dateFrom + " " + this.dateTo + " " + this.vehicle.vin;
  }
}
