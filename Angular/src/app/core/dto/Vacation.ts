import {Company} from "./Company";
import {User} from "./User";
import {Driver} from "./Driver";

export class Vacation {

  public id;

  public dateFrom;

  public dateTo;

  public reason;

  public driver: User;

  constructor(id, dateFrom, dateTo, reason) {

    this.id = id;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.reason = reason;
  }

  static getEmptyVacation() : Vacation {
    return new Vacation(null, null,null,
      null);
  }

  public validate() : boolean{
        return this.id != null && this.dateFrom != null && this.dateTo != null;
  }

  public toString(): string {

    return this.driver.name + " " + this.driver.surname + " " + this.id + " " + this.dateFrom + " " + this.dateTo + " " + this.reason;
  }
}
