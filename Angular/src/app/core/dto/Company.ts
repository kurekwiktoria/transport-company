export class Company{

  public nip;

  public name;

  public city;

  public street;

  public phone;

  public fax;

  public email;

  public regon;

  public krs;

  public zipCode;

  constructor(nip, name, city, street, phone, fax, email, regon, krs, postalCode) {
    this.nip = nip;
    this.name = name;
    this.city = city;
    this.street = street;
    this.phone = phone;
    this.fax = fax;
    this.email = email;
    this.regon = regon;
    this.krs = krs;
    this.zipCode = postalCode;
  }

  static getEmptyCompany() : Company {
    return new Company(null, null,null,null,null,null,null,null,null, null);
  }

  public validate() : boolean {
    return this.nip!=null && this.name!=null && this.city!=null && this.street!+null && this.email!=null && this.regon!=null && this.krs!=null;
  }

}
