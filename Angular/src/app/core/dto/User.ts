import {Company} from "./Company";
import {UserTypeEnum} from "../enums/UserTypeEnum";
import {Firm} from "./Firm";

export class User {

  public pesel;

  public login;

  public password;

  public name;

  public surname;

  public email;

  public type: UserTypeEnum;

  public idNumber;

  public firm: Firm;

  constructor(pesel, login, password, name, surname, email, type) {
    this.pesel = pesel;
    this.login = login;
    this.password = password;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.type = type;
  }

  static getEmptyUser() : User {
    return new User(null, null,null,
      null, null, null, null);
  }

  public validate() : boolean{
        return this.pesel != null && this.name != null && this.email != null &&
          this.password != null && this.login != null && this.surname != null && this.type != null;
  }

  public toString(): string {

    return this.pesel + " " + this.login + " " + this.password + " " +
    this.type +  " " +this.surname + " " +this.email + " " +this.name + this.firm.nip;
  }
}
