export class Firm {

  public nip;

  public name;

  public city;

  public street;

  public zipCode;

  public phone;

  public fax;

  public regon;

  public email;

  public krs;

  constructor(nip, name, city, street, zipCode, phone, fax, regon, email, krs) {
    this.nip = nip;
    this.name = name;
    this.city = city;
    this.street = street;
    this.zipCode = zipCode;
    this.phone = phone;
    this.fax = fax;
    this.regon = regon;
    this.email = email;
    this.krs = krs;
  }

  static getEmptyFirm(): Firm {
    return new Firm(null, null, null,
      null, null, null, null, null, null, null);
  }

  public validate(): boolean {
    return this.nip != null && this.name != null && this.city != null && this.street != null && this.zipCode != null &&
      this.phone != null && this.fax != null && this.regon != null && this.email != null && this.krs != null;
  }

  public toString(): string {

    return this.nip + " " + this.name + " " + this.city + " " + this.street + " " + this.zipCode + " " + this.phone + " " +
      this.fax + " " + this.regon + " " + this.email + " " + this.krs;
  }

  public equalsss(firm: Firm): boolean {
    if ( this.nip != firm.nip)
      return false;
    else if (this.name != firm.name)
      return false;
    else if (this.city != firm.city)
      return false;
    else if (this.street != firm.street)
      return false;
    else if (this.zipCode != firm.zipCode)
      return false;
    else if (this.phone != firm.phone)
      return false;
    else if (this.fax != firm.fax)
      return false;
    else if (this.email != firm.email)
      return false;
    // else if (this.regon != firm.regon)
    //   return false;
    // else if (this.krs != firm.krs)
    //   return false;
    else return true;
  }
}
