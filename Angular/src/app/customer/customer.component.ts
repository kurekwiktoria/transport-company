import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  public name : string;
  public email : string;
  public pesel : string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if ( this.authService.user != null){
        this.name = this.authService.user.name + " " + this.authService.user.surname;
        this.email = this.authService.user.email;
        this.pesel = this.authService.user.pesel;
      }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
