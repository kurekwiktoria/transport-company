import { Component, OnInit } from '@angular/core';
import {OrderService} from "../../services/order/order.service";
import {AuthService} from "../../services/auth/auth.service";
import {MatTableDataSource} from "@angular/material";

@Component({
  selector: 'app-show-orders',
  templateUrl: './show-orders.component.html',
  styleUrls: ['./show-orders.component.css']
})
export class ShowOrdersComponent implements OnInit {

  public displayedColumns = ['index', 'loadingPlace', 'destination', 'loadingDate', 'unloadingDate', 'distance', 'price'];

  constructor(private orderService: OrderService, private authService: AuthService) { }

  public datasource;

  ngOnInit() {
    if(this.authService.user != null){
      this.orderService.getAllOrdersByUserPesel(this.authService.user.pesel)
        .subscribe( data => this.datasource = new MatTableDataSource(data));
    }
  }

  refresh(){
    if(this.authService.user != null){
      this.orderService.getAllOrdersByUserPesel(this.authService.user.pesel)
        .subscribe( data => this.datasource = new MatTableDataSource(data));
    }
  }

}
