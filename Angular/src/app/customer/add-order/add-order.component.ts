import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../services/order/order.service";
import {AuthService} from "../../services/auth/auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserTypeEnum} from "../../core/enums/UserTypeEnum";
import {Order} from "../../core/dto/Order";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {

  private lastOrder : Order;
  constructor(private orderService: OrderService, private authService: AuthService, private userService: UserService) { }

  orderForm : FormGroup;

  ngOnInit() {
    this.orderService.getLastOrder().subscribe( orderData => this.lastOrder = orderData);
    this.orderForm = new FormGroup({
      loadingPlace: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      destination: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      loadingDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      unloadingDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      distance: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      price: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(null, [
        Validators.required,
        Validators.minLength(5)
      ]),
    })
  }

  saveOrderClick(){
    console.log("Zapisuje!");
    if(this.authService.user != null && this.authService.user.type == UserTypeEnum.CLIENT){
      if(this.orderForm.valid){
      console.log("ZAPISUJE");
        const order: Order = this.orderForm.value;
        order.user = this.authService.user;
        order.isFinished = 0;
        order.id = this.lastOrder.id + 1;
        this.orderService.addOrder(order).then(data => {console.log(data)}, err => console.log(err));
      }
    }
  }

}
