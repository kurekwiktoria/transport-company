import { Component, OnInit } from '@angular/core';
import {MatDialog, MatTableDataSource} from "@angular/material";
import {UserService} from "../services/user/user.service";
import {User} from "../core/dto/User";
import {PageEvent} from "@angular/material/typings/paginator";
import {AddTruckDialogComponent} from "../shipper/show-trucks/add-truck-dialog/add-truck-dialog.component";
import {AddUserDialogComponent} from "./add-user-dialog/add-user-dialog.component";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService,
              public dialog: MatDialog) {
  }

  public user: User[];

  public datasource;

  public displayedColumns = ['index', 'name', 'surname', 'email', 'login', 'pesel', 'idNumber', 'nip', 'type', 'action'];

  public length : Number;

  public pageSize : number;

  public pageSizeOptions : number[] = [12, 25, 50, 100];

  public pageIndex : number = 0;

  public pageEvent : PageEvent;


  ngOnInit() {
    this.pageSize = this.pageSizeOptions[0];
    this.userService.getAll(1, this.pageSize).subscribe(data => this.datasource = new MatTableDataSource(data));
    this.userService.getAllCount().subscribe(data => this.length = data);
  }

  deleteUserClick(pesel: string) {
    this.userService.delete(pesel).subscribe();
  }

  onPageEvent(event : PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.userService.getAll(event.pageIndex, event.pageSize).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.userService.getAll(this.pageIndex, this.pageSize).subscribe(data => this.datasource.data = data);
  }

  onOpenAddUserDialog() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      // data:
    });

    this.onCloseAddUserDialog(dialogRef);
  }

  onCloseAddUserDialog(dialogRef) {

  }
  //
  // onEditAddUserDialog() {
  //   const dialogRef = this.dialog.open(AddUserDialogComponent, {
  //     data:
  //   });
  // }
}
