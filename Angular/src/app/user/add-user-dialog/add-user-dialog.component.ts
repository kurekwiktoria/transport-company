import {Component, Inject, OnInit} from '@angular/core';
import {User} from "../../core/dto/User";
import {UserService} from "../../services/user/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Driver} from "../../core/dto/Driver";


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.css']
})
export class AddUserDialogComponent implements OnInit {

  constructor(private userService: UserService,
              public dialogRef: MatDialogRef<AddUserDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

  emailForm = new FormControl('', [Validators.required, Validators.email]);

  nameForm = new FormControl('', Validators.required);

  userForm : FormGroup;

  companyForm : FormGroup;

  accountForm: FormGroup;

  driverForm: FormGroup;

  endText: string = '';

  ngOnInit() {
      this.accountForm = new FormGroup({
        login: new FormControl(null, [
          Validators.required,
          Validators.minLength(4)
        ]),
        password: new FormControl(null, [
          Validators.required,
          Validators.minLength(4)
        ]),
        email: new FormControl(null, [
          Validators.required,
          Validators.email
        ]),
        type: new FormControl(null, [
          Validators.required,
          Validators.minLength(4)
        ])
      });

    this.userForm = new FormGroup({
      pesel: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      surname: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      idNumber: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    });

    this.driverForm = new FormGroup({
      drivingLicenceExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      medicalExaminationExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      state: new FormControl(null, [
        Validators.required,
      ])
    });

    this.companyForm = new FormGroup({
      nip: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      city: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      street: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      phone: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      fax: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.email
      ]),
      regon: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      krs: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      zipCode: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  saveUserClick() {
    let user : User = this.userForm.value;
    if (this.userForm.valid) {
      console.log("Wysłano" + user);
      this.userService.addUser(user);
    }
    else {
      console.log("Walidacja" + user.name);
    }

  }

  saveForwarderClick() {
    let user : User = this.userForm.value;
    user.firm = this.companyForm.value;
    if (this.userForm.valid && this.companyForm.valid) {
      this.userService.addForwarder(user);
      console.log("Wysłano forwardera" + user.toString());
    }
  }

  getErrorMessage() {
    return this.emailForm.hasError('required') ? 'To pole jest wymagane, wprowadź wartość' :
      this.emailForm.hasError('email') ? 'E-mail nie jest poprawny' :
        '';
  }

  submitForwarder() {
    let user: User = this.accountForm.value;
    user.name = this.userForm.get('name').value;
    user.surname = this.userForm.get('surname').value;
    user.idNumber = this.userForm.get('idNumber').value;
    user.pesel = this.userForm.get('pesel').value;
    user.firm = this.companyForm.value;

    console.log(user.toString());

    if (this.userForm.valid && this.accountForm.valid && this.companyForm.valid) {
      this.userService.saveForwarder(user).subscribe(data => {
        console.log(data);
        this.endText = 'Dodano spedytora do bazy.'
      }, error1 => console.log(error1));
      console.log("Wysłano forwardera" + user.toString());
    }
  }

  submitUser() {
    let user: User = this.accountForm.value;
    user.name = this.userForm.get('name').value;
    user.surname = this.userForm.get('surname').value;
    user.idNumber = this.userForm.get('idNumber').value;
    user.pesel = this.userForm.get('pesel').value;

    console.log('Dodaję usera: ' + user.pesel + ' ' + user.login + ' '  + user.password + ' ' +
      user.type + ' '  +user.surname + ' ' +user.email + ' '  + user.name);

    if (this.userForm.valid && this.accountForm.valid) {

      this.userService.saveUser(user).subscribe(data => {
        console.log(data);
        this.endText = 'Dodano zleceniodawce do bazy.'
        }, error1 => console.log(error1)
      );
      console.log("Wysłano clienta" + user.toString());
    }
  }

  submitDriver() {
    let user: Driver = this.accountForm.value;
    user.name = this.userForm.get('name').value;
    user.surname = this.userForm.get('surname').value;
    user.idNumber = this.userForm.get('idNumber').value;
    user.pesel = this.userForm.get('pesel').value;
    user.medicalExaminationExpiryDate = this.driverForm.get('medicalExaminationExpiryDate').value;
    user.drivingLicenceExpiryDate = this.driverForm.get('drivingLicenceExpiryDate').value;
    user.state = this.driverForm.get('state').value;

    console.log('Dodaję drivera: ' + user.pesel + ' ' + user.login + ' '  + user.password + ' ' +
      user.type + ' '  +user.surname + ' ' +user.email + ' '  + user.name);

    if (this.userForm.valid && this.accountForm.valid) {

      this.userService.saveDriver(user).subscribe(data => {
        console.log(data);
        this.endText = 'Dodano kierowcę do bazy.'
      }, error1 => console.log(error1)
      );
      console.log("Wysłano drivera" + user.toString());
    }
  }

}
