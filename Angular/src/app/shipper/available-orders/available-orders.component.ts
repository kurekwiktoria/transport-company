import { Component, OnInit } from '@angular/core';
import {OrderService} from "../../services/order/order.service";
import {MatTableDataSource} from "@angular/material";
import {Router} from "@angular/router";
import {PassingService} from "../../services/passing/passing.service";
import {Order} from "../../core/dto/Order";

@Component({
  selector: 'app-available-orders',
  templateUrl: './available-orders.component.html',
  styleUrls: ['./available-orders.component.css']
})
export class AvailableOrdersComponent implements OnInit {

  constructor(private orderService: OrderService,
              private passingService: PassingService,
              private router: Router) { }

  public displayedColumns = ['index', 'loadingPlace', 'destination', 'loadingDate', 'unloadingDate', 'distance', 'price', 'action'];

  public datasource;

  ngOnInit() {
    this.orderService.getAllAvailableOrders().subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.orderService.getAllAvailableOrders().subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  onAcceptOrder(order: Order) {
    this.passingService.order = order;
    this.router.navigate(['/shipper/available-orders/accept-order/']);
  }

}
