import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../../services/order/order.service";
import {UserService} from "../../../services/user/user.service";
import {AuthService} from "../../../services/auth/auth.service";
import {User} from "../../../core/dto/User";
import {Vehicle} from "../../../core/dto/Vehicle";
import {Transport} from "../../../core/dto/Transport";
import {VehicleService} from "../../../services/vehicle/vehicle.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserTypeEnum} from "../../../core/enums/UserTypeEnum";
import {TransportService} from "../../../services/transport/transport.service";
import {Order} from "../../../core/dto/Order";
import {PassingService} from "../../../services/passing/passing.service";

@Component({
  selector: 'app-accept-order',
  templateUrl: './accept-order.component.html',
  styleUrls: ['./accept-order.component.css']
})
export class AcceptOrderComponent implements OnInit {

  constructor(private orderService: OrderService,
              private userService: UserService,
              private authService: AuthService,
              private vehicleService: VehicleService,
              private passingService: PassingService,
              private transportService: TransportService) {

  }

  private nip : string;
  private order: Order;
  public user: User[];
  public drivers: User[];
  public vehicles: Vehicle[];
  public lastTransport : Transport;
  transportForm : FormGroup;

  ngOnInit() {
    this.transportService.getLastTransport().subscribe( transportData => this.lastTransport = transportData);
    this.order = this.passingService.order;

    this.transportForm = new FormGroup({
      driverSalary: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      driver: new FormControl(null, [
        Validators.required
      ]),
      vehicle: new FormControl(null, [
        Validators.required
      ])
    });

    console.log(this.order.id + ' ' + this.order.description + ' ' + this.order.destination);
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null ) {
        this.nip = this.authService.user.firm.nip;
        this.userService.getAllDriversByFirm(this.nip).subscribe( data => this.drivers = data)
        this.vehicleService.getAllVehiclesByFirmNip(this.nip).subscribe( vehicleData => this.vehicles = vehicleData);
      }


  }

  saveTransportClick(){
    if ( this.authService.user != null && this.authService.user.type == UserTypeEnum.FORWARDER ) {
      this.nip = this.authService.user.firm.nip;
      if (this.transportForm.valid) {
        let transport: Transport = this.transportForm.value;
        transport.id = this.lastTransport.id + 1;
        // transport.id = 1220;
        transport.order = this.order;
        transport.profit = this.order.price - transport.driverSalary;
        this.transportService.saveTransport(transport).subscribe(data => console.log(data),
          error1 => console.log(error1));
      }
    }
  }

}
