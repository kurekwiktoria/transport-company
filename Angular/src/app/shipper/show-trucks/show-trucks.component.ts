import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../services/vehicle/vehicle.service";
import {MatDialog, MatTableDataSource} from "@angular/material";
import {AddTruckDialogComponent} from "./add-truck-dialog/add-truck-dialog.component";
import {Vehicle} from "../../core/dto/Vehicle";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-show-trucks',
  templateUrl: './show-trucks.component.html',
  styleUrls: ['./show-trucks.component.css']
})
export class ShowTrucksComponent implements OnInit {

  constructor(private vehicleService: VehicleService,
              private authService: AuthService,
              public dialog: MatDialog) { }

  public  displayedColumns = ['index', 'brand', 'model', 'state', 'tankCapacity','registrationNumber', 'combustion', 'vehicleCapacity', 'power', 'vin', 'mileage', 'type', 'serviceCheckExpiryDate', 'insuranceExpiryDate', 'action'];

  public datasource;

  // default nip
  private nip: string = '679-893-47-93';

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null )
        this.nip = this.authService.user.firm.nip;

    this.vehicleService.getAllVehiclesByFirmNip(this.nip).subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  loadData(){
    this.vehicleService.getAllVehiclesByFirmNip(this.nip).subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.vehicleService.getAllVehiclesByFirmNip(this.nip).subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  deleteVehicle(vin : string){
    this.vehicleService.delete(vin).subscribe(data => this.loadData());
  }

  onEditVehicle(vehicle: Vehicle) {

  }

  onOpenAddTruckDialog() {
    const dialogRef = this.dialog.open(AddTruckDialogComponent, {
      // data:
    });

    this.onCloseAddTruckDialog(dialogRef);
  }

  onCloseAddTruckDialog(dialogRef) {
    dialogRef.afterClosed().subscribe( result => {
      console.log('Zamykam okno dodawania ciężarówki');
    }, err => {
      console.log('Error okna ' + err);
    })
  }

}
