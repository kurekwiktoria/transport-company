import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTrucksComponent } from './show-trucks.component';

describe('ShowTrucksComponent', () => {
  let component: ShowTrucksComponent;
  let fixture: ComponentFixture<ShowTrucksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTrucksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTrucksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
