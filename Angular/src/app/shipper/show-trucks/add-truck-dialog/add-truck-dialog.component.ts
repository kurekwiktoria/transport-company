import {Component, Inject, OnInit} from '@angular/core';
import {VehicleService} from "../../../services/vehicle/vehicle.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Vehicle} from "../../../core/dto/Vehicle";
import {Firm} from "../../../core/dto/Firm";
import {AuthService} from "../../../services/auth/auth.service";
import {UserTypeEnum} from "../../../core/enums/UserTypeEnum";

@Component({
  selector: 'app-add-truck-dialog',
  templateUrl: './add-truck-dialog.component.html',
  styleUrls: ['./add-truck-dialog.component.css']
})
export class AddTruckDialogComponent implements OnInit {

  constructor(private vehicleService: VehicleService,
              private authService: AuthService,
              public dialogRef: MatDialogRef<AddTruckDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

  vehicleForm : FormGroup;

  ngOnInit() {
    this.vehicleForm = new FormGroup({
      vin: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      brand: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      model: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      power: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      tankCapacity: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      vehicleCapacity: new FormControl(null, [
        Validators.required
      ]),
      serviceCheckExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      insuranceExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      mileage: new FormControl(null, [
        Validators.required
      ]),
      type: new FormControl(null, [
        Validators.required
      ]),
      registrationNumber: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
    })
  }

  saveVehicleClick() {
    const user = this.authService.user;
    let nip;
    if ( user != null && user.type == UserTypeEnum.FORWARDER )
    {
      nip = user.firm.nip;
      if ( this.vehicleForm.valid ) {
        const vehicle: Vehicle = this.vehicleForm.value;
        vehicle.combustion = 0;
        vehicle.state = 'AVAILABLE';

        const firm = Firm.getEmptyFirm();
        firm.nip = nip;
        vehicle.firm = firm;

        this.vehicleService.addVehicle(vehicle).subscribe(data => {
            console.log(data);
          },
          err => console.log(err));
      }
    }


  }

}
