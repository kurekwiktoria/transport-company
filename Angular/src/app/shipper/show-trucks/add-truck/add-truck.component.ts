import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../../services/vehicle/vehicle.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Vehicle} from "../../../core/dto/Vehicle";
import {Firm} from "../../../core/dto/Firm";
import {FirmService} from "../../../services/firm/firm.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-add-truck',
  templateUrl: './add-truck.component.html',
  styleUrls: ['./add-truck.component.css']
})
export class AddTruckComponent implements OnInit {

  constructor(private vehicleService: VehicleService, private firmService: FirmService) { }

  vehicleForm : FormGroup;

  ngOnInit() {
    this.vehicleForm = new FormGroup({
      brand: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      model: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      power: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      tankCapacity: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      vehicleCapacity: new FormControl(null, [
        Validators.required
      ]),
      serviceCheckExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      insuranceExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
      mileage: new FormControl(null, [
        Validators.required
      ]),
      type: new FormControl(null, [
        Validators.required
      ]),
      registrationNumber: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ]),
    })
  }

  saveVehicleClick() {
    let vehicle : Vehicle = this.vehicleForm.value;
    // let firm : Firm = this.firmService.getFirmEntityByNip("679-893-47-93").subscribe( data => {firm = data});
    if (this.vehicleForm.valid) {
      console.log("Wysłano" + vehicle);
      this.vehicleService.addVehicle(vehicle);
    } else {
      console.log("Walidacja" + vehicle.brand);
    }
  }

}
