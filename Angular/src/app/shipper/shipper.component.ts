import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-shipper',
  templateUrl: './shipper.component.html',
  styleUrls: ['./shipper.component.css']
})
export class ShipperComponent implements OnInit {

  public name : string;
  public email : string;
  public pesel : string;
  public firmName : string;
  public firmAdress : string;
  public city :string;
  public nip : string;
  public firmEmail : string;
  public fax : string;
  public firmPhone : string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null ) {
        this.name = this.authService.user.name + " " + this.authService.user.surname;
        this.email = this.authService.user.email;
        this.pesel = this.authService.user.pesel;
        this.firmName = this.authService.user.firm.name;
        this.firmAdress = this.authService.user.firm.street;
        this.city = this.authService.user.firm.zipCode + " " + this.authService.user.firm.city;
        this.firmPhone = this.authService.user.firm.phone;
        this.nip = this.authService.user.firm.nip;
        this.firmEmail = this.authService.user.firm.email;
        this.fax = this.authService.user.firm.fax;
      }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
