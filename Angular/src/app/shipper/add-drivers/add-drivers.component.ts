import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {User} from "../../core/dto/User";
import {MatTableDataSource} from "@angular/material";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-add-drivers',
  templateUrl: './add-drivers.component.html',
  styleUrls: ['./add-drivers.component.css']
})
export class AddDriversComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService) {
  }

  public user: User[];

  public datasource;

  public displayedColumns = ['index', 'name', 'surname', 'email', 'login', 'action'];


  ngOnInit() {
    this.userService.getAllDriversWithoutFirm().subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  loadData(){
    this.userService.getAllDriversWithoutFirm().subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  // deleteUserClick(pesel: string) {
  //   this.userService.delete(pesel).subscribe();
  // }

  refresh() {
    this.userService.getAllDriversWithoutFirm().subscribe(data => this.datasource.data = data);
  }

  makeDriverHired(pesel: string) {
    this.userService.makeDriverHired(pesel, this.authService.user.firm.nip).subscribe(data => this.loadData());
  }
}
