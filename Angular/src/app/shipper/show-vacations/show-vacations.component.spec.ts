import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowVacationsComponent } from './show-vacations.component';

describe('ShowVacationsComponent', () => {
  let component: ShowVacationsComponent;
  let fixture: ComponentFixture<ShowVacationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowVacationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowVacationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
