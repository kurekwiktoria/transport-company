import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {AuthService} from "../../services/auth/auth.service";
import {VacationService} from "../../services/vacation/vacation.service";

@Component({
  selector: 'app-show-vacations',
  templateUrl: './show-vacations.component.html',
  styleUrls: ['./show-vacations.component.css']
})
export class ShowVacationsComponent implements OnInit {

  public displayedColumns = ['index', 'name', 'surname', 'dateFrom', 'dateTo', 'reason'];

  constructor(private vacationService: VacationService,
              private authService: AuthService) { }

  public datasource;

  private nip : string;

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null )
        this.nip = this.authService.user.firm.nip;
    this.vacationService.getVacationsByDriverFirmNip(this.nip).subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.vacationService.getVacationsByDriverFirmNip(this.nip).subscribe(
      data => this.datasource = new MatTableDataSource(data));
  }
}
