import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.css']
})
export class ShowDetailsComponent implements OnInit {

  displayedColumns = ['index', 'placeStart', 'destination', 'dateStart', 'dateEnd', 'distance', 'burnedFuel', 'driverSalary', 'profit', 'finished', 'action'];


  constructor() { }

  ngOnInit() {
  }

}
