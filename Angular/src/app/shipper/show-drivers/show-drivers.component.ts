import { Component, OnInit , Inject} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {User} from "../../core/dto/User";
import {MatTableDataSource} from "@angular/material";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-show-drivers',
  templateUrl: './show-drivers.component.html',
  styleUrls: ['./show-drivers.component.css']
})
export class ShowDriversComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService) {
  }

  public user: User[];

  public loadComponent = false;

  public datasource;

  //possible columns
  cols = new Map([
    [1, 'index'],
    [2, 'name'],
    [3, 'surname'],
    [4, 'email'],
    [5, 'login'],
    [6, 'pesel'],
    [7, 'idNumber'],
    [8, 'state'],
    [9, 'action'],
    [10, 'stateDescr'],
  ]);

  public displayedColumns = ['index', 'name', 'surname', 'email', 'login', 'pesel', 'idNumber', 'state', 'action'];

  private nip : string;

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null )
        this.nip = this.authService.user.firm.nip;
    this.userService.getAllDriversByFirm(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));

    this.displayedColumns = [ this.cols.get(1), this.cols.get(2), this.cols.get(3), this.cols.get(4), this.cols.get(5), this.cols.get(6), this.cols.get(10), this.cols.get(9)];



  }

  loadData() {
    this.userService.getAllDriversByFirm(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  deleteUserClick(pesel: string) {
    this.userService.delete(pesel).subscribe();
  }

  refresh() {
    // this.userService.getAllDrivers().subscribe(data => this.datasource.data = data);
    this.userService.getAllDriversByFirm(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  makeDriverFired(pesel: string) {
    this.userService.makeDriverFired(pesel).subscribe(data => this.loadData());

  }

  loadDetails(){
    this.loadComponent = true;
  }

}
