import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {UserService} from "../../services/user/user.service";
import {AuthService} from "../../services/auth/auth.service";
import {TransportService} from "../../services/transport/transport.service";

@Component({
  selector: 'app-show-transports',
  templateUrl: './show-transports.component.html',
  styleUrls: ['./show-transports.component.css']
})
export class ShowTransportsComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService,
              private transportService: TransportService) {
  }

  public transport : Transport[];

  public loadComponent = false;

  public datasource;

  private nip : string;

  public displayedColumns = ['index', 'relation', 'burnedFuel', 'driver', 'driverSalary', 'profit'];

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null ){
        this.nip = this.authService.user.firm.nip;
        this.transportService.getAllTransportsByFirmNip(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));
      }


  }

  loadData() {
    this.transportService.getAllTransportsByFirmNip(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.transportService.getAllTransportsByFirmNip(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

}
