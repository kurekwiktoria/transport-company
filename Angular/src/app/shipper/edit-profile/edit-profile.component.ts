import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user/user.service";
import {User} from "../../core/dto/User";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  name : string;
  surname : string;
  password: string;
  email:string;
  idnumber:string;

  constructor(private userService: UserService, private authService : AuthService) { }

  emailForm = new FormControl('', [Validators.required, Validators.email]);

  userForm : FormGroup;

  ngOnInit() {
    this.name = this.authService.user.name;
    this.surname = this.authService.user.surname;
    this.password = this.authService.user.password;
    this.email = this.authService.user.email;
    this.idnumber = this.authService.user.idNumber;

    this.userForm = new FormGroup({
      password: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      surname: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(null, [
        // Validators.required,
        Validators.email
      ]),
      idNumber: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ])
    });

  }

  saveUserClick() {
    let user : User = this.userForm.value;
    let activeUser : User = this.authService.user;
    if (this.userForm.valid) {
      console.log("Wysłano" + user);
      activeUser.email = user.email != null ? user.email : activeUser.email;
      activeUser.surname = user.surname != null ? user.surname : activeUser.surname;
      activeUser.name = user.name != null ? user.name : activeUser.name;
      activeUser.idNumber = user.idNumber!= null ? user.idNumber : activeUser.idNumber;
      activeUser.password = user.password != null ? user.password : activeUser.password;
      this.userService.updateUser(this.authService.user.pesel, activeUser);
    }
    else {
      console.log("Walidacja" + user.name);
    }
  }


  getErrorMessage() {
    return this.emailForm.hasError('required') ? 'To pole jest wymagane, wprowadź wartość' :
      this.emailForm.hasError('email') ? 'E-mail nie jest poprawny' :
        '';
  }

}
