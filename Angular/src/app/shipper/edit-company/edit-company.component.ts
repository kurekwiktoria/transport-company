import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Firm} from "../../core/dto/Firm";
import {FirmService} from "../../services/firm/firm.service";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.css']
})
export class EditCompanyComponent implements OnInit {

  emailForm = new FormControl('', [Validators.required, Validators.email]);

  nameForm = new FormControl('', Validators.required);

  companyForm : FormGroup;

  name : string;
  city:string;
  zipcode:string;
  street:string;
  fax:string;
  email:string;
  phone:string;

  constructor(private firmService : FirmService,
              private authService : AuthService,
              private router: Router) { }

  ngOnInit() {
    this.name = this.authService.user.firm.name;
    this.city = this.authService.user.firm.city;
    this.zipcode = this.authService.user.firm.zipCode;
    this.street = this.authService.user.firm.street;
    this.fax = this.authService.user.firm.fax;
    this.email = this.authService.user.firm.email;
    this.phone = this.authService.user.firm.phone;

    this.companyForm = new FormGroup({
      name: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      city: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      street: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      phone: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      fax: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4),
        Validators.email
      ]),
      zipCode: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ])
    });
  }


  saveForwarderClick() {
    let firm : Firm = this.companyForm.value;
    let activeUserFirm : Firm = this.authService.user.firm;
    firm.nip = activeUserFirm.nip;

    if (this.companyForm.valid) {
      console.log("Wysłano" + firm);
      activeUserFirm.name = firm.name != null ? firm.name : activeUserFirm.name;
      activeUserFirm.city = firm.city != null ? firm.city : activeUserFirm.city;
      activeUserFirm.zipCode = firm.zipCode != null ? firm.zipCode : activeUserFirm.zipCode;
      activeUserFirm.street = firm.street != null ? firm.street : activeUserFirm.street;
      activeUserFirm.fax = firm.fax != null ? firm.fax : activeUserFirm.fax;
      activeUserFirm.email = firm.email != null ? firm.email : activeUserFirm.email;
      activeUserFirm.phone = firm.phone != null ? firm.phone : activeUserFirm.phone;
      this.firmService.updateFirm(activeUserFirm).subscribe( updatedFirm => {
        console.log(updatedFirm);
        this.router.navigate(['/shipper'])},
        err => console.log(err));
    }
    else {
      console.log("Walidacja" + firm.name);
    }
  }

  getErrorMessage() {
    return this.emailForm.hasError('required') ? 'To pole jest wymagane, wprowadź wartość' :
      this.emailForm.hasError('email') ? 'E-mail nie jest poprawny' :
        '';
  }

}
