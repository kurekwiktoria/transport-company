import { Component } from '@angular/core';
import {TruckService} from "./services/truck/truck.service";
import {Truck} from "./core/dto/Truck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';

  constructor(private truckService: TruckService){}

  truck: Truck;

  ngOnInit() {
  }
}
