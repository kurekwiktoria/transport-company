import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './core/routing/app-routing.module';
import {
  MatTableModule, MatInputModule, MatSidenavModule,//
  MatToolbarModule, MatListModule, MatIconModule,//
  MatButtonModule, MatMenuModule, MatDatepickerModule, MatPaginatorModule, MatDialogModule, MatNativeDateModule
} from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainNavComponent } from './core/main-nav/main-nav.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddUserDialogComponent } from './user/add-user-dialog/add-user-dialog.component';
import { ShipperComponent } from './shipper/shipper.component';
import { DriverComponent } from './driver/driver.component';
import { LoginComponent } from './login/login.component';
import { CustomerComponent } from './customer/customer.component';
import { RegisterComponent } from './register/register.component';
import { ShowDriversComponent } from './shipper/show-drivers/show-drivers.component';
import { ShowTransportsComponent } from './shipper/show-transports/show-transports.component';
import { AvailableOrdersComponent } from './shipper/available-orders/available-orders.component';
import { ShowTrucksComponent } from './shipper/show-trucks/show-trucks.component';
import { ShowVacationsComponent } from './shipper/show-vacations/show-vacations.component';
import { AddDriversComponent } from './shipper/add-drivers/add-drivers.component';
import { EditProfileComponent } from './shipper/edit-profile/edit-profile.component';
import { EditCompanyComponent } from './shipper/edit-company/edit-company.component';
import { ReportFailureComponent } from './driver/report-failure/report-failure.component';
import { ShowDetailsComponent } from './shipper/show-drivers/show-details/show-details.component';
import { VacationRequestComponent } from './driver/vacation-request/vacation-request.component';
import { EditDriverComponent } from './driver/edit-driver/edit-driver.component';
import { AddOrderComponent } from './customer/add-order/add-order.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { ShowOrdersComponent } from './customer/show-orders/show-orders.component';
import { ShowTransportHistoryComponent } from './driver/show-transport-history/show-transport-history.component';
import { ShowStatisticsComponent } from './driver/show-statistics/show-statistics.component';
import { AddTruckComponent } from './shipper/show-trucks/add-truck/add-truck.component';
import { AcceptOrderComponent } from './shipper/available-orders/accept-order/accept-order.component';
import { FinishOrderComponent } from './driver/show-transport-history/finish-order/finish-order.component';
import { AddTruckDialogComponent } from './shipper/show-trucks/add-truck-dialog/add-truck-dialog.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatStepperModule} from '@angular/material/stepper';
import {ExtendDrivingLicenceDialogComponent} from "./driver/extend-driving-licence-dialog/extend-driving-licence-dialog.component";
import { ExtendMedicalExaminationDialogComponent } from './driver/extend-medical-examination-dialog/extend-medical-examination-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    DashboardComponent,
    MainNavComponent,
    AddUserDialogComponent,
    MainNavComponent,
    ShipperComponent,
    DriverComponent,
    LoginComponent,
    CustomerComponent,
    RegisterComponent,
    ShowDriversComponent,
    ShowTransportsComponent,
    AvailableOrdersComponent,
    ShowTrucksComponent,
    ShowVacationsComponent,
    AddDriversComponent,
    EditProfileComponent,
    EditCompanyComponent,
    ReportFailureComponent,
    ShowDetailsComponent,
    VacationRequestComponent,
    EditDriverComponent,
    AddOrderComponent,
    EditCustomerComponent,
    ShowOrdersComponent,
    ShowTransportHistoryComponent,
    ShowStatisticsComponent,
    AddTruckComponent,
    AcceptOrderComponent,
    FinishOrderComponent,
    AddTruckDialogComponent,
    ExtendDrivingLicenceDialogComponent,
    ExtendMedicalExaminationDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    NoopAnimationsModule,
    LayoutModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatMenuModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatDialogModule,
    FlexLayoutModule,
    MatNativeDateModule,
    MatStepperModule
  ],
  entryComponents: [
    AddTruckDialogComponent,
    ExtendDrivingLicenceDialogComponent,
    ExtendMedicalExaminationDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
