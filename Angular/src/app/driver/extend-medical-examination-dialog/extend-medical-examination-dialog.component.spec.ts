import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendMedicalExaminationDialogComponent } from './extend-medical-examination-dialog.component';

describe('ExtendMedicalExaminationDialogComponent', () => {
  let component: ExtendMedicalExaminationDialogComponent;
  let fixture: ComponentFixture<ExtendMedicalExaminationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendMedicalExaminationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendMedicalExaminationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
