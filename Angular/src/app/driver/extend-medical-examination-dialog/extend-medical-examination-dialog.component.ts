import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {AuthService} from "../../services/auth/auth.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../core/dto/User";

@Component({
  selector: 'app-extend-medical-examination-dialog',
  templateUrl: './extend-medical-examination-dialog.component.html',
  styleUrls: ['./extend-medical-examination-dialog.component.css']
})
export class ExtendMedicalExaminationDialogComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService,
              public dialogRef: MatDialogRef<ExtendMedicalExaminationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

    medicalExaminationForm : FormGroup;

  ngOnInit() {
    this.medicalExaminationForm = new FormGroup( {
      medicalExaminationExpiryDate: new FormControl( null, [
        Validators.required,
        Validators.minLength(4)
      ])
    })
  }

  saveNewDateClick(){
    let user : User = this.authService.user;
    if(this.medicalExaminationForm.valid){
      this.userService.updateMedicalExamination(this.medicalExaminationForm.get("medicalExaminationExpiryDate").value, user.pesel).subscribe( data => console.log(data));
    }
  }

}
