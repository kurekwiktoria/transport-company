import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";
import {MatDialog, MatTableDataSource} from "@angular/material";
import {ExtendDrivingLicenceDialogComponent} from "./extend-driving-licence-dialog/extend-driving-licence-dialog.component";
import {Driver} from "../core/dto/Driver";
import {UserService} from "../services/user/user.service";
import {ExtendMedicalExaminationDialogComponent} from "./extend-medical-examination-dialog/extend-medical-examination-dialog.component";
import {TransportService} from "../services/transport/transport.service";

export interface PeriodicElement {
  dateStart: string;
  dateEnd: string;
  distance: number;
  placeStart: string;
  placeEnd: string;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    dateStart: '12/11/2018',
    dateEnd: '13/11/2018',
    distance: 123,
    placeStart: 'Kraków',
    placeEnd: 'Zakopane',
    description: `Ananasy dla Lidla`
  },   {
    dateStart: '14/11/2018',
    dateEnd: '15/11/2018',
    distance: 342,
    placeStart: 'Zakopane',
    placeEnd: 'Wrocław',
    description: `Arbuzy dla Carrefoura`
  },  {
    dateStart: '16/11/2018',
    dateEnd: '17/11/2018',
    distance: 478,
    placeStart: 'Wrocław',
    placeEnd: 'Warszawa',
    description: `Bakłażany dla Tesco`
  },
];

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DriverComponent implements OnInit {

  dataSource;
  columnsToDisplay = ['loadingDate', 'unloadingDate', 'loadingPlace', 'destination'];
  labels = ['Data startu', 'Przewidywana data zakończenia', 'Miejsce startu', 'Miejsce zakończenia'];
  expandedElement: PeriodicElement;

  public name : string;
  public email : string;
  public pesel : string;
  public medicalExaminationExpiryDate : string;
  public drivingLicenceExpiryDate : string;
  public driver: Driver;
  constructor(private authService: AuthService, private router: Router, public dialog: MatDialog, private userService: UserService,
              private transportService: TransportService) { }

  ngOnInit() {
    if ( this.authService.user != null){
        this.name = this.authService.user.name + " " + this.authService.user.surname;
        this.email = this.authService.user.email;
        this.pesel = this.authService.user.pesel;
        this.userService.getDriverByPesel(this.authService.user.pesel).subscribe( driverData => this.driver = driverData);
        this.transportService.getFutureTransportsByDriverPesel(this.authService.user.pesel).subscribe( transportData => this.dataSource = new MatTableDataSource(transportData));
      }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  onOpenExtendDrivingLicenceDialog() {
    const dialogRef = this.dialog.open(ExtendDrivingLicenceDialogComponent, {
      // data:
    });

    this.onCloseExtendDrivingLicenceDialog(dialogRef);
  }

  onCloseExtendDrivingLicenceDialog(dialogRef) {
    dialogRef.afterClosed().subscribe( result => {
      console.log('Zamykam okno przedłużenia prawa jazdy');
    }, err => {
      console.log('Error okna ' + err);
    })
  }

  onOpenExtendMedicalExaminationDialog() {
    const dialogRef = this.dialog.open(ExtendMedicalExaminationDialogComponent, {
      // data:
    });

    this.onCloseExtendMediacalExaminationDialog(dialogRef);
  }

  onCloseExtendMediacalExaminationDialog(dialogRef) {
    dialogRef.afterClosed().subscribe( result => {
      console.log('Zamykam okno przedłużenia prawa jazdy');
    }, err => {
      console.log('Error okna ' + err);
    })
  }

}
