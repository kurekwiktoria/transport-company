import { Component, OnInit } from '@angular/core';
import { MatDatepicker} from "@angular/material";
import {OrderService} from "../../services/order/order.service";
import {UserService} from "../../services/user/user.service";
import {AuthService} from "../../services/auth/auth.service";
import {VehicleService} from "../../services/vehicle/vehicle.service";
import {TransportService} from "../../services/transport/transport.service";
import {UserTypeEnum} from "../../core/enums/UserTypeEnum";
import {Transport} from "../../core/dto/Transport";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Vacation} from "../../core/dto/Vacation";
import {VacationService} from "../../services/vacation/vacation.service";

@Component({
  selector: 'app-vacation-request',
  templateUrl: './vacation-request.component.html',
  styleUrls: ['./vacation-request.component.css']
})
export class VacationRequestComponent implements OnInit {

  private nip : string;
  vacationForm : FormGroup;
  lastVacation : Vacation;

  constructor(private orderService: OrderService, private userService: UserService, private authService: AuthService,
              private vacationService: VacationService, private transportService: TransportService) {

  }

  ngOnInit() {
    this.vacationService.getLastVacation().subscribe( vacationData => this.lastVacation = vacationData);

    this.vacationForm = new FormGroup({
      dateFrom: new FormControl(null, [
        // Validators.required,
        Validators.minLength(10)
      ]),
      dateTo: new FormControl(null, [
        // Validators.required
        Validators.minLength(10)
      ]),
      reason: new FormControl(null, [
        // Validators.required,
        Validators.minLength(4)
      ])
    });

    if ( this.authService.user != null)
      if ( this.authService.user.firm != null ) {
        this.nip = this.authService.user.firm.nip;
      }
  }

  saveClick(){
    if ( this.authService.user != null) {
      this.nip = this.authService.user.firm.nip;
      if (this.vacationForm.valid) {
        const vacation : Vacation = this.vacationForm.value;
        vacation.id = this.lastVacation.id + 1;
        vacation.driver = this.authService.user;
        this.vacationService.addVacation(vacation);
      }
    }
  }
}
