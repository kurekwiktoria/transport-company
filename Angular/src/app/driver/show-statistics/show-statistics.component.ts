import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {AuthService} from "../../services/auth/auth.service";
import {TransportService} from "../../services/transport/transport.service";
import {Transport} from "../../core/dto/Transport";

interface Statistics {
  salaryAvg;
  distanceSum;
  combustionAvg;
  burnedFuel;
}

@Component({
  selector: 'app-show-statistics',
  templateUrl: './show-statistics.component.html',
  styleUrls: ['./show-statistics.component.css']
})
export class ShowStatisticsComponent implements OnInit {

  displayedColumns = ['index', 'loadingPlace', 'destination', 'distance', 'driverSalary', 'burnedFuel'];


  constructor(private authService: AuthService,
              private transportService: TransportService) { }

  nip: string;

  datasource;

  stats: Statistics;

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null )
        this.nip = this.authService.user.firm.nip;




    this.transportService.getAllTransportsByFirmNip(this.nip).subscribe(data => {
      let stat: Statistics = {burnedFuel: 0, combustionAvg: 0, distanceSum: 0, salaryAvg: 0};

      for (let transport of data) {
        stat.burnedFuel += transport.burnedFuel;
        stat.distanceSum += transport.order.distance;
        stat.salaryAvg += transport.driverSalary;
      }

      stat.burnedFuel = stat.burnedFuel / data.length;
      stat.distanceSum = stat.distanceSum / data.length;
      stat.salaryAvg = stat.salaryAvg / data.length;

      this.stats = stat;

      let stats: Statistics[] = [];
      stats.push(stat);
      this.datasource = new MatTableDataSource(data);
      // this.datasource = new MatTableDataSource(stats);
      }, error1 => console.log(error1));


  }


}
