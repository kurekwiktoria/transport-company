import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../services/vehicle/vehicle.service";
import {AuthService} from "../../services/auth/auth.service";
import {Vehicle} from "../../core/dto/Vehicle";
import {RepairService} from "../../services/repair/repair.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Repair} from "../../core/dto/Repair";

@Component({
  selector: 'app-report-failure',
  templateUrl: './report-failure.component.html',
  styleUrls: ['./report-failure.component.css']
})
export class ReportFailureComponent implements OnInit {

  constructor(private vehicleService: VehicleService, private authService: AuthService, private repairService: RepairService) { }

  public vehicles: Vehicle[];
  private vehicleFailure: Vehicle;
  private nip : string;
  private lastRepair : Repair;
  private repairedVehicle : Vehicle;

  repairForm : FormGroup;

  ngOnInit() {
    if( this.authService.user != null)
      if(this.authService.user.firm != null){
        this.nip = this.authService.user.firm.nip;
        this.vehicleService.getAllVehiclesByFirmNip(this.nip).subscribe( data => this.vehicles = data);
        this.repairService.getLastRepair().subscribe( repairData =>  this.lastRepair = repairData);
      }

      this.repairForm = new FormGroup({
        vehicleVin: new FormControl( null, [
          Validators.required,
          Validators.minLength(4)
        ]),
        cost: new FormControl( null, [
          Validators.required
        ]),
        description: new FormControl( null, [
          Validators.required,
          Validators.minLength(4)
        ]),
        dateFrom: new FormControl( null, [
          Validators.required,
          Validators.minLength(4)
        ]),
        dateTo: new FormControl( null, [
          Validators.required,
          Validators.minLength(4)
        ]),
      })

  }

  reportFailureClick(){
    if( this.authService.user != null)
      if(this.authService.user.firm != null){
        let repair: Repair = this.repairForm.value;
        repair.id = this.lastRepair.id + 1;
        this.repairedVehicle = Vehicle.getEmptyVehicle();
        this.repairedVehicle.vin = this.repairForm.get('vehicleVin').value;
        repair.vehicle = this.repairedVehicle;

        this.repairService.addRepair(repair);
      }
  }

  getVehicle(){
    this.vehicleService.getVehicleByVin(this.repairForm.get('vehicleVin').value).subscribe( vehicleData => this.repairedVehicle = vehicleData);
  }

}
