import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTransportHistoryComponent } from './show-transport-history.component';

describe('ShowTransportHistoryComponent', () => {
  let component: ShowTransportHistoryComponent;
  let fixture: ComponentFixture<ShowTransportHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTransportHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTransportHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
