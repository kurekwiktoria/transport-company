import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {UserService} from "../../services/user/user.service";
import {TransportService} from "../../services/transport/transport.service";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-show-transport-history',
  templateUrl: './show-transport-history.component.html',
  styleUrls: ['./show-transport-history.component.css']
})
export class ShowTransportHistoryComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService,
              private transportService: TransportService) {
  }

  public loadComponent = false;

  public datasource;

  private nip : string;

  public displayedColumns = ['index', 'placeStart', 'destination', 'dateStart', 'dateEnd', 'distance'];

  ngOnInit() {
    if ( this.authService.user != null)
      if ( this.authService.user.firm != null )
        this.nip = this.authService.user.firm.nip;
      this.transportService.getAllTransportsByDriverPesel(this.authService.user.pesel).subscribe(data => this.datasource = new MatTableDataSource(data));
    // this.transportService.getAllTransportsByFirmNip(this.nip).subscribe(data => this.datasource = new MatTableDataSource(data));

  }

  loadData() {
    this.transportService.getAllTransportsByDriverPesel(this.authService.user.pesel).subscribe(data => this.datasource = new MatTableDataSource(data));
  }

  refresh() {
    this.transportService.getAllTransportsByDriverPesel(this.authService.user.pesel).subscribe(data => this.datasource = new MatTableDataSource(data));

  }


}
