import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {AuthService} from "../../services/auth/auth.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {MatDialog} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../core/dto/User";
import {Driver} from "../../core/dto/Driver";

@Component({
  selector: 'app-extend-driving-licence-dialog',
  templateUrl: './extend-driving-licence-dialog.component.html',
  styleUrls: ['./extend-driving-licence-dialog.component.css']
})
export class ExtendDrivingLicenceDialogComponent implements OnInit {

  constructor(private userService: UserService,
              private authService: AuthService,
              public dialogRef: MatDialogRef<ExtendDrivingLicenceDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

       drivingLicenceForm : FormGroup;

  ngOnInit() {
    this.drivingLicenceForm = new FormGroup({
      drivingLicenceExpiryDate: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    })
  }

  saveNewDateClick(){
    let user : User = this.authService.user;
    if(this.drivingLicenceForm.valid){
      this.userService.updateDrivingLicence(this.drivingLicenceForm.get("drivingLicenceExpiryDate").value, user.pesel).subscribe( data => console.log(data));
    }
  }

}
