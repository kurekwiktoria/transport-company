import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendDrivingLicenceDialogComponent } from './extend-driving-licence-dialog.component';

describe('ExtendDrivingLicenceDialogComponent', () => {
  let component: ExtendDrivingLicenceDialogComponent;
  let fixture: ComponentFixture<ExtendDrivingLicenceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendDrivingLicenceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendDrivingLicenceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
