import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";
import {UserTypeEnum} from "../core/enums/UserTypeEnum";
import {User} from "../core/dto/User";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  loginForm: FormGroup;

  ngOnInit() {
    this.loginForm = new FormGroup({
      login: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  onLogIn() {
    if ( this.loginForm.valid) {
      const login = this.loginForm.controls['login'].value;
      const password = this.loginForm.controls['password'].value;

      this.authService.logIn(login).subscribe( (user: User) => {
        if ( user.idNumber != null && user.password === password ){
          console.log('Zalogowano jako ' + user.name + ' ' + user.surname + ', typ: ' + user.type);
          this.authService.user = user;
          const userType: UserTypeEnum = user.type;

          if ( userType == UserTypeEnum.FORWARDER) {
            console.log('logged as shipper');
            this.router.navigate(['/shipper']);
          } else if ( userType == UserTypeEnum.DRIVER) {
            console.log('logged as driver');
            this.router.navigate(['/driver']);
          } else if ( userType == UserTypeEnum.CLIENT) {
            console.log('logged as customer');
            this.router.navigate(['/customer']);
          } else if ( userType == UserTypeEnum.ADMIN) {
            console.log('logged as admin');
            this.router.navigate(['']);
          } else {
            console.log('Nie udalo sie rozpoznac typu');
          }
        }
      }, err => console.log(err));
    }
  }

}
