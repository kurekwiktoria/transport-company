package com.transport.spring.exception;

/**
 * Wyjątek rzucany przy próbie wykonania operacji z nieprawidłowymi parametrami, np. brak wymaganych atrybutów obiektu przy jego
 * tworzeniu lub ich nieprawidłowy format.
 */
public class InvalidArgumentException extends TransportCompanyException {
    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }
}

