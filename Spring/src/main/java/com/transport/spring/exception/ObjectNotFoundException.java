package com.transport.spring.exception;

/**
 * Wyjątek rzucany w sytuacji, gdy nie odnaleziono obiektu, którego istnienia oczekujemy
 */
public class ObjectNotFoundException extends TransportCompanyException {
    public ObjectNotFoundException(String message) {
        super(message);
    }

    public ObjectNotFoundException(String massage, Throwable e) {
        super(massage, e);
    }
}