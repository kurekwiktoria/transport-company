package com.transport.spring.exception;

/**
 * Wyjątek rzucany w sytuacji, gdy nie odnaleziono encji, której istnienia oczekujemy
 */
public class EntityNotFoundException extends ObjectNotFoundException {
    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String massage, Throwable e) {
        super(massage, e);
    }
}
