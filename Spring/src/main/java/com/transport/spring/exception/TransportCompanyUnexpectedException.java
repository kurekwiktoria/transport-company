package com.transport.spring.exception;
/**
 * Wyjątek typu runtime oznaczający niespodziewaną z punktu widzenia wykonywanego kodu sytuację, na którą nie można sensownie
 * zareagować.
 *
 * Dodatkowo ukrywa prawdziwy message, zachowując orginalną przyczynę w cause.
 *
 * Wyjątek typu Stackless
 *
 * <br/><br/><b>!NIGDY NIE POWINIEN BYĆ RZUCANY SAMODZIELNIE, TYLKO RETHROW!</b><br/>
 *
 */

public final class TransportCompanyUnexpectedException extends TransportCompanyStacklessException {
    public TransportCompanyUnexpectedException(Throwable cause) {
        this(cause.getMessage(), cause);
    }

    public TransportCompanyUnexpectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getRawMessage() {
        return super.getMessage();
    }

    @Override
    public String getMessage() {
        return "Błąd o id: " + getId();
    }
}