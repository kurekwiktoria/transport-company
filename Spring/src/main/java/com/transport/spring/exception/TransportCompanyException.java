package com.transport.spring.exception;

import com.transport.spring.utils.StringUtils;

/**
 * Wyjątek bazowy dla aplikacji Vega.
 * Wszystkie pozostałe zdefiniowane w aplikacji wyjątki powinny pośrednio lub bezpośrednio po nim dziedziczyć.
 */
public abstract class TransportCompanyException extends RuntimeException {
    private final String id;

    public TransportCompanyException(String message) {
        super(message);

        id = StringUtils.encodeErrorId(System.currentTimeMillis());
    }

    public TransportCompanyException(Throwable cause) {
        this(cause.getMessage(), cause);
    }

    public TransportCompanyException(String message, Throwable cause) {
        super(message, cause);

        if (cause instanceof TransportCompanyException)
            //propagujemy id z wyjątku przyczynowego
            id = ((TransportCompanyException) cause).getId();
        else
            id = StringUtils.encodeErrorId(System.currentTimeMillis());
    }

    public String getId() {
        return id;
    }
}
