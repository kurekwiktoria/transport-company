package com.transport.spring.exception;

/**
 * Wyjątek bazowy dla funkcyjnych wyjątków w aplikacji Vega.
 *
 * Wyjątek funkcyjny, nie posiada stacktrace
 *
 * Wyjątek typu Stackless, służący tylko do opakowywania innych wyjątków jeżeli nie chcemy mieć informacji w stacktrace o samym
 * przepakowaniu. W naszym przypadku idealna jeżeli chcemy mieć pewność że obcy wyjątek otrzyma ID. Sprawdza sie również jeżeli
 * wyjątki mają zadanie funkcyjne i nie potrzebny jest ich stacktrace.
 * <br/><br/><a href="http://fahdshariff.blogspot.com/2012/01/stackless-exceptions-for-improved.html">http://fahdshariff.blogspot.com/2012/01/stackless-exceptions-for-improved.html</a>
 * <br/><a href="http://cretesoft.com/archive/newsletter.do?issue=129">http://cretesoft.com/archive/newsletter.do?issue=129</a>
 */
public abstract class TransportCompanyStacklessException extends TransportCompanyException {
    protected TransportCompanyStacklessException(String message) {
        super(message);
    }

    protected TransportCompanyStacklessException(Throwable cause) {
        super(cause);
    }

    protected TransportCompanyStacklessException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public Throwable fillInStackTrace() {
        //blokujemy uzupełnienie stacktrace
        return this;
    }
}
