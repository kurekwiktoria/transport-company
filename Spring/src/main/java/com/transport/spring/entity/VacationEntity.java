package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = VacationEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class VacationEntity {

    public static final String TABLE_NAME = "vacation";
    public static final String COL_ID = "id";
    private static final String COL_DATE_FROM = "date_from";
    private static final String COL_DATE_TO = "date_to";
    private static final String COL_REASON = "reason";
    private static final String REFCOL_DRIVER_PESEL = "driver_pesel";

    @Id
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_DATE_FROM)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateFrom;

    @Column(name = COL_DATE_TO)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateTo;

    @Column(name = COL_REASON)
    private String reason;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = REFCOL_DRIVER_PESEL, referencedColumnName = DriverEntity.COL_PESEL, foreignKey = @ForeignKey(name="driver_pesel"))
    private DriverEntity driver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }
}
