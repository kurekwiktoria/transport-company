package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transport.spring.enums.VehicleStateEnum;
import com.transport.spring.enums.VehicleTypeEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = VehicleEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class VehicleEntity {

    public static final String TABLE_NAME = "vehicle";
    public static final String COL_VIN = "vin";
    private static final String COL_BRAND = "brand";
    private static final String COL_MODEL = "model";
    private static final String COL_POWER = "power";
    private static final String COL_TANK_CAPACITY = "tank_capacity";
    private static final String COL_COMBUSTION = "combustion";
    private static final String COL_VEHICLE_CAPACITY = "vehicle_capacity";
    private static final String COL_SERVICE_CHECK_EXPIRY_DATE = "service_check_expiry_date";
    private static final String COL_INSURANCE_EXPIRY_DATE = "insurance_expiry_date";
    private static final String COL_STATE = "state";
    private static final String COL_MILEAGE = "mileage";
    private static final String COL_TYPE = "type";
    private static final String COL_REGISTRATION_NUMBER = "registration_number";
    private static final String REFCOL_FIRM_NIP = "firm_nip";

    @Id
    @Column(name = COL_VIN)
    private String vin;

    @Column(name = COL_BRAND)
    private String brand;

    @Column(name = COL_MODEL)
    private String model;

    @Column(name = COL_POWER)
    private Long power;

    @Column(name = COL_TANK_CAPACITY)
    private Long tankCapacity;

    @Column(name = COL_COMBUSTION)
    private Long combustion;

    @Column(name = COL_VEHICLE_CAPACITY)
    private Long vehicleCapacity;

    @Column(name = COL_SERVICE_CHECK_EXPIRY_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date serviceCheckExpiryDate;

    @Column(name = COL_INSURANCE_EXPIRY_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date insuranceExpiryDate;

    @Column(name = COL_STATE)
    @Enumerated(EnumType.STRING)
    private VehicleStateEnum state;

    @Transient
    @JsonSerialize
    private String stateDescr;

    @Column(name = COL_MILEAGE)
    private Long mileage;

    @Column(name = COL_TYPE)
    @Enumerated(EnumType.STRING)
    private VehicleTypeEnum type;

    @Transient
    @JsonSerialize
    private String typeDescr;

    @Column(name = COL_REGISTRATION_NUMBER)
    private String registrationNumber;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = REFCOL_FIRM_NIP, referencedColumnName = FirmEntity.COL_NIP, foreignKey = @ForeignKey(name="firm_nip"))
    private FirmEntity firm;

    @PostLoad
    void setDescriptions()
    {
        this.stateDescr = this.state.getHumanReadableDescription();
        this.typeDescr = this.type.getHumanReadableDescription();
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    public Long getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(Long tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public Long getCombustion() {
        return combustion;
    }

    public void setCombustion(Long combustion) {
        this.combustion = combustion;
    }

    public Long getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(Long vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public Date getServiceCheckExpiryDate() {
        return serviceCheckExpiryDate;
    }

    public void setServiceCheckExpiryDate(Date serviceCheckExpiryDate) {
        this.serviceCheckExpiryDate = serviceCheckExpiryDate;
    }

    public Date getInsuranceExpiryDate() {
        return insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(Date insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    public VehicleStateEnum getState() {
        return state;
    }

    public void setState(VehicleStateEnum state) {
        this.state = state;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public FirmEntity getFirm() {
        return firm;
    }

    public void setFirm(FirmEntity firm) {
        this.firm = firm;
    }
}
