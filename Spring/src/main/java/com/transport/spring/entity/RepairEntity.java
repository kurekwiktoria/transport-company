package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = RepairEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RepairEntity {

    public static final String TABLE_NAME = "repair";
    public static final String COL_ID = "id";
    private static final String COL_COST = "cost";
    private static final String COL_DESCRIPTION = "description";
    private static final String COL_DATE_FROM = "date_from";
    private static final String COL_DATE_TO = "date_to";
    private static final String REFCOL_VEHICLE_VIN = "vehicle_vin";

    @Id
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_COST)
    private Long cost;

    @Column(name = COL_DESCRIPTION)
    private String description;

    @Column(name = COL_DATE_FROM)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateFrom;

    @Column(name = COL_DATE_TO)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateTo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = REFCOL_VEHICLE_VIN, referencedColumnName = VehicleEntity.COL_VIN, foreignKey = @ForeignKey(name="vehicle_vin"))
    private VehicleEntity vehicle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public VehicleEntity getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleEntity vehicle) {
        this.vehicle = vehicle;
    }
}
