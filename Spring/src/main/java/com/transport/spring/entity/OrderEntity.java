package com.transport.spring.entity;

import ch.qos.logback.core.net.server.Client;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = OrderEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class OrderEntity {

    public static final String TABLE_NAME = "orders";
    public static final String COL_ID = "id";
    private static final String COL_LOADING_PLACE = "loading_place";
    private static final String COL_DESTINATION = "destination";
    private static final String COL_LOADING_DATE = "loading_date";
    private static final String COL_UNLOADING_DATE = "unloading_date";
    private static final String COL_IS_ACCEPTED = "is_finished";
    private static final String COL_DISTANCE = "distance";
    private static final String COL_PRICE = "price";
    private static final String COL_DESCRIPTION = "description";
    private static final String REFCOL_USER_PESEL = "user_pesel";

    @Id
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_LOADING_PLACE)
    private String loadingPlace;

    @Column(name = COL_DESTINATION)
    private String destination;

    @Column(name = COL_LOADING_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date loadingDate;

    @Column(name = COL_UNLOADING_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date unloadingDate;

    @Column(name = COL_IS_ACCEPTED)
    private Boolean isAccepted;

    @Column(name = COL_DISTANCE)
    private Long distance;

    @Column(name = COL_PRICE)
    private Float price;

    @Column(name = COL_DESCRIPTION)
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = REFCOL_USER_PESEL, referencedColumnName = ClientEntity.COL_PESEL, foreignKey = @ForeignKey(name="user_pesel"))
    private ClientEntity user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoadingPlace() {
        return loadingPlace;
    }

    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(Date loadingDate) {
        this.loadingDate = loadingDate;
    }

    public Date getUnloadingDate() {
        return unloadingDate;
    }

    public void setUnloadingDate(Date unloadingDate) {
        this.unloadingDate = unloadingDate;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClientEntity getUser() {
        return user;
    }

    public void setUser(ClientEntity user) {
        this.user = user;
    }
}

