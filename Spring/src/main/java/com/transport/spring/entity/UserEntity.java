package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transport.spring.enums.UserTypeEnum;

import javax.persistence.*;


@Entity
@Table(name = UserEntity.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserEntity {

    public static final String TABLE_NAME = "user";
    public static final String COL_PESEL = "pesel";
    private static final String COL_LOGIN = "login";
    private static final String COL_PASSWORD = "password";
    private static final String COL_NAME = "name";
    private static final String COL_SURNAME = "surname";
    private static final String COL_EMAIL = "email";
    private static final String COL_ID_NUMBER = "id_number";
    private static final String REFCOL_FIRM_NIP = "firm_nip";

    @Id
    @Column(name = COL_PESEL)
    private String pesel;

    @Column(name = COL_LOGIN)
    private String login;

    @Column(name = COL_PASSWORD)
    private String password;

    @Column(name = COL_NAME)
    private String name;

    @Column(name = COL_SURNAME)
    private String surname;

    @Column(name = COL_EMAIL)
    private String email;

    @Column(name = COL_ID_NUMBER)
    private String idNumber;

    @Transient
    @JsonSerialize
    private UserTypeEnum type;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinColumn(name = REFCOL_FIRM_NIP, referencedColumnName = FirmEntity.COL_NIP, foreignKey = @ForeignKey(name="firm_nip"))
    private FirmEntity firm;


    public UserEntity(String pesel, String login, String password, String name,//
                      String surname, String email, String idNumber) {
        this.pesel = pesel;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
    }

    public UserEntity() {
    }

    @PostLoad
    private void regcognizeType() {
        DiscriminatorValue annotation = this.getClass().getAnnotation(DiscriminatorValue.class);

        if ( annotation != null )
            this.type = UserTypeEnum.valueOf(annotation.value());
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public FirmEntity getFirm() {
        return firm;
    }

    public void setFirm(FirmEntity firm) {
        this.firm = firm;
    }

}
