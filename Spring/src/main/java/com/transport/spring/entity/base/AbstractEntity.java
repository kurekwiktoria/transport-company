package com.transport.spring.entity.base;

import com.transport.spring.enums.Described;

import java.util.Date;

/**
 * Abstrakcyjna bazowa klasa dla wszystkich encji, wraz ze skonfigurowaną sekwencją
 * TODO: Uzupełnić po dodaniu hibernate
 */
//@MappedSuperclass
@SuppressWarnings("UnusedDeclaration")
public abstract class AbstractEntity implements Persistable, Described {
    public static final String MIN_SEQUENCE_VALUE = "500000000";

    public static final String SEQUENCE = "sequence";

    public static final String COL_ID = "id";

    public static final String COL_VERSION = "_version";

    public static final String COL_CREATION_DATE = "creation_date";

    public static final String COL_MODIFICATION_DATE = "modification_date";

//    @Id
//    @Column(name = COL_ID, nullable = false)
//    @GeneratedValue(generator = SEQUENCE)
//    @GenericGenerator(//
//            name = SEQUENCE,//
//            strategy = "com.transport.spring.entity.base.Generator",//
//            parameters = {//
//                    @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = AbstractEntity.SEQUENCE),//
//                    @Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = AbstractEntity.MIN_SEQUENCE_VALUE)//
//            }//
//    )
    private Long id;

//    @Version
//    @Column(name = COL_VERSION, nullable = false)
    private Long versionNumber;

//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = COL_CREATION_DATE, nullable = false)
    private Date creationDate;

//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = COL_MODIFICATION_DATE, nullable = false)
    private Date modificationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Long version) {
        this.versionNumber = version;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

//    @PrePersist
    protected void onCreate() {
        creationDate = new Date();
        modificationDate = new Date();
    }

//    @PreUpdate
    protected void onUpdate() {
        modificationDate = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractEntity)) {
            return false;
        }

        AbstractEntity other = (AbstractEntity) o;

        final Long id = getId();
        final Long otherId = other.getId();
        return !(id != null ? !id.equals(otherId) : otherId != null);
    }

    @Override
    public int hashCode() {
        final Long id = getId();
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String getHumanReadableDescription() {
        return this.toString();
    }

    /**
     * Porównaj instancje encji, uwzględniając wersje
     *
     * @param o instancja encji
     *
     * @return <code>true</code> jeżeli zgadza się id oraz wersja, inaczej <code>false</code>
     */
    public boolean equalsWithVersion(Object o) {
        if (equals(o)) {
            // sprawdz wersję
            AbstractEntity other = (AbstractEntity) o;

            final Long versionNumber = getId();
            final Long otherVersionNumber = other.getId();
            return !(versionNumber != null ? !versionNumber.equals(otherVersionNumber) : otherVersionNumber != null);
        }
        return false;
    }
}

