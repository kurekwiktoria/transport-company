package com.transport.spring.entity.base;

import java.io.Serializable;

/**
 * Interfejs dla wszystkich encji
 */
public interface Persistable extends Serializable {
}
