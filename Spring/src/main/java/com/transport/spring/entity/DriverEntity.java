package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transport.spring.enums.DriverStateEnum;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue("DRIVER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DriverEntity extends UserEntity {

    private static final String COL_DRIVING_LICENCE_EXPIRY_DATE = "driving_licence_expiry_date";
    private static final String COL_MEDICAL_EXAMINATION_EXPIRY_DATE = "medical_examination_expiry_date";
    private static final String COL_STATE = "state";

    @Column(name = COL_DRIVING_LICENCE_EXPIRY_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date drivingLicenceExpiryDate;

    @Column(name = COL_MEDICAL_EXAMINATION_EXPIRY_DATE)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date medicalExaminationExpiryDate;

    @Column(name = COL_STATE)
    @Enumerated(EnumType.STRING)
    private DriverStateEnum state;

    @Transient
    @JsonSerialize
    private String stateDescr;

    public Date getDrivingLicenceExpiryDate() {
        return drivingLicenceExpiryDate;
    }

    public void setDrivingLicenceExpiryDate(Date drivingLicenceExpiryDate) {
        this.drivingLicenceExpiryDate = drivingLicenceExpiryDate;
    }

    public Date getMedicalExaminationExpiryDate() {
        return medicalExaminationExpiryDate;
    }

    public void setMedicalExaminationExpiryDate(Date medicalExaminationExpiryDate) {
        this.medicalExaminationExpiryDate = medicalExaminationExpiryDate;
    }

    public DriverStateEnum getState() {
        return state;
    }

    public void setState(DriverStateEnum state) {
        this.state = state;
        this.stateDescr = state.getHumanReadableDescription();
    }

    public String getStateDescr() {
        return stateDescr;
    }

    public void setStateDescr(String stateDescr) {
        this.stateDescr = stateDescr;
    }

    @PostLoad
    private void setStateDescr() {
        this.stateDescr = state.getHumanReadableDescription();
    }
}
