package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = TransportEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TransportEntity {

    public static final String TABLE_NAME = "transport";
    public static final String COL_ID = "id";
    private static final String COL_BURNED_FUEL = "burned_fuel";
    private static final String COL_DRIVER_SALARY = "driver_salary";
    private static final String COL_PROFIT = "profit";
    private static final String COL_IS_FINISHED = "is_finished";
    private static final String REFCOL_VEHICLE_VIN = "vehicle_vin";
    private static final String REFCOL_DRIVER_PESEL = "driver_pesel";

    @Id
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_BURNED_FUEL)
    private Long burnedFuel;

    @Column(name = COL_DRIVER_SALARY)
    private Long driverSalary;

    @Column(name = COL_PROFIT)
    private Long profit;

    @Column(name = COL_IS_FINISHED)
    private Boolean isFinished;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name="order_id"))
    private OrderEntity order;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = REFCOL_VEHICLE_VIN, referencedColumnName = VehicleEntity.COL_VIN, foreignKey = @ForeignKey(name="vehicle_vin"))
    private VehicleEntity vehicle;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "driver_pesel", foreignKey = @ForeignKey(name="driver_pesel"))
    private DriverEntity driver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBurnedFuel() {
        return burnedFuel;
    }

    public void setBurnedFuel(Long burnedFuel) {
        this.burnedFuel = burnedFuel;
    }

    public Long getDriverSalary() {
        return driverSalary;
    }

    public void setDriverSalary(Long driverSalary) {
        this.driverSalary = driverSalary;
    }

    public Long getProfit() {
        return profit;
    }

    public void setProfit(Long profit) {
        this.profit = profit;
    }

    public Boolean getFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public VehicleEntity getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleEntity vehicle) {
        this.vehicle = vehicle;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }
}
