package com.transport.spring.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = FirmEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FirmEntity {

    public static final String TABLE_NAME = "firm";
    public static final String COL_NIP = "nip";
    private static final String COL_NAME = "name";
    private static final String COL_CITY = "city";
    private static final String COL_STREET = "street";
    private static final String COL_STREET_NUMBER = "street_number";
    private static final String COL_HOUSE_NUMBER = "house_number";
    private static final String COL_ZIP_CODE = "zip_code";
    private static final String COL_PHONE = "phone";
    private static final String COL_FAX = "fax";
    private static final String COL_REGON = "regon";
    private static final String COL_EMAIL = "email";
    private static final String COL_KRS = "krs";

    @Id
    @Column(name = COL_NIP)
    private String nip;

    @Column(name = COL_NAME)
    private String name;

    @Column(name = COL_CITY)
    private String city;

    @Column(name = COL_STREET)
    private String street;

    @Column(name = COL_STREET_NUMBER)
    private Long streetNumber;

    @Column(name = COL_HOUSE_NUMBER)
    private Long houseNumber;

    @Column(name = COL_ZIP_CODE)
    private String zipCode;

    @Column(name = COL_PHONE)
    private String phone;

    @Column(name = COL_FAX)
    private String fax;

    @Column(name = COL_REGON)
    private String regon;

    @Column(name = COL_EMAIL)
    private String email;

    @Column(name = COL_KRS)
    private String krs;

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Long streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Long getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Long houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKrs() {
        return krs;
    }

    public void setKrs(String krs) {
        this.krs = krs;
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj instanceof FirmEntity) {
            FirmEntity firm = (FirmEntity) obj;
            if ( this.nip.equals(firm.nip))
                return true;
        }

        return false;
    }
}
