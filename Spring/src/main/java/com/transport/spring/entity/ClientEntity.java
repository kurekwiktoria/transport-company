package com.transport.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("CLIENT")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClientEntity extends UserEntity {

}
