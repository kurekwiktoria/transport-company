
ALTER TABLE `order` RENAME TO `orders`;

ALTER TABLE `transport` ADD `driver_pesel` VARCHAR(20) NOT NULL;

ALTER TABLE `transport` ADD CONSTRAINT `transport_ibfk_3` FOREIGN KEY (`driver_pesel`) REFERENCES `user` (`pesel`);

