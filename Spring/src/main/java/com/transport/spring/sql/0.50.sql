
--
-- Baza danych: `transportcompany`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `driver`
--

CREATE TABLE `driver` (
  `pesel` varchar(11) COLLATE utf16_polish_ci NOT NULL,
  `driving_licence_expiry_date` date NOT NULL,
  `medical_examination_expiry_date` date NOT NULL,
  `state` varchar(255) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `firm`
--

CREATE TABLE `firm` (
  `nip` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `name` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `city` varchar(255) ,
  `street` varchar(255) ,
  `street_number` int(11) DEFAULT NULL,
  `house_number` int(11) DEFAULT NULL,
  `zip_code` varchar(6) ,
  `phone` varchar(12) COLLATE utf16_polish_ci NOT NULL,
  `fax` varchar(40) ,
  `regon` varchar(14) COLLATE utf16_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `krs` varchar(10)
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `loading_place` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `destination` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `loading_date` date NOT NULL,
  `unloading_date` date NOT NULL,
  `is_finished` tinyint(1) NOT NULL,
  `distance` int(11) NOT NULL,
  `price` float NOT NULL,
  `description` varchar(255) ,
  `user_pesel` varchar(11) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `repair`
--

CREATE TABLE `repair` (
  `id` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `description` varchar(255) ,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `vehicle_vin` varchar(17) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `burned_fuel` int(11) DEFAULT NULL,
  `driver_salary` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `is_finished` tinyint(1) NOT NULL,
  `order_id` int(11) NOT NULL,
  `vehicle_vin` varchar(17) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `pesel` varchar(11) COLLATE utf16_polish_ci NOT NULL,
  `login` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `name` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `type` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `id_number` varchar(9) COLLATE utf16_polish_ci NOT NULL,
  `firm_nip` varchar(10) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vacation`
--

CREATE TABLE `vacation` (
  `id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `reason` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `driver_pesel` varchar(11) COLLATE utf16_polish_ci NOT NULL
) ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vehicle`
--

CREATE TABLE `vehicle` (
  `vin` varchar(17) COLLATE utf16_polish_ci NOT NULL,
  `brand` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `model` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `power` int(11) NOT NULL,
  `tank_capacity` int(11) NOT NULL,
  `combustion` int(11) NOT NULL,
  `vehicle_capacity` int(11) NOT NULL,
  `service_check_expiry_date` date NOT NULL,
  `insurance_expiry_date` date NOT NULL,
  `state` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `mileage` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf16_polish_ci NOT NULL,
  `registration_number` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `firm_nip` varchar(10) COLLATE utf16_polish_ci NOT NULL
) ;

--
-- Indeksy dla tabeli `driver`
--
ALTER TABLE `driver`
  ADD UNIQUE KEY `pesel` (`pesel`);

--
-- Indeksy dla tabeli `firm`
--
ALTER TABLE `firm`
  ADD PRIMARY KEY (`nip`);

--
-- Indeksy dla tabeli `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_pesel` (`user_pesel`);

--
-- Indeksy dla tabeli `repair`
--
ALTER TABLE `repair`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_vin` (`vehicle_vin`);

--
-- Indeksy dla tabeli `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`),
  ADD KEY `vehicle_vin` (`vehicle_vin`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`pesel`),
  ADD KEY `firm_nip` (`firm_nip`);

--
-- Indeksy dla tabeli `vacation`
--
ALTER TABLE `vacation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver_pesel` (`driver_pesel`);

--
-- Indeksy dla tabeli `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vin`),
  ADD KEY `firm_nip` (`firm_nip`);

--
-- Ograniczenia dla tabeli `driver`
--
ALTER TABLE `driver`
  ADD CONSTRAINT `driver_ibfk_1` FOREIGN KEY (`pesel`) REFERENCES `user` (`pesel`);

--
-- Ograniczenia dla tabeli `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`user_pesel`) REFERENCES `user` (`pesel`);

--
-- Ograniczenia dla tabeli `repair`
--
ALTER TABLE `repair`
  ADD CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`vehicle_vin`) REFERENCES `vehicle` (`vin`);

--
-- Ograniczenia dla tabeli `transport`
--
ALTER TABLE `transport`
  ADD CONSTRAINT `transport_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  ADD CONSTRAINT `transport_ibfk_2` FOREIGN KEY (`vehicle_vin`) REFERENCES `vehicle` (`vin`);

--
-- Ograniczenia dla tabeli `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`firm_nip`) REFERENCES `firm` (`nip`);

--
-- Ograniczenia dla tabeli `vacation`
--
ALTER TABLE `vacation`
  ADD CONSTRAINT `vacation_ibfk_1` FOREIGN KEY (`driver_pesel`) REFERENCES `driver` (`pesel`);

--
-- Ograniczenia dla tabeli `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`firm_nip`) REFERENCES `firm` (`nip`);