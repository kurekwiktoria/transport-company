ALTER TABLE `driver` DROP FOREIGN KEY `driver_ibfk_1`;
ALTER TABLE `vacation DROP FOREIGN KEY `vacation_ibfk_1`;
DROP TABLE `driver`;
ALTER TABLE `user` ADD COLUMN `driving_licence_expiry_date` date;
ALTER TABLE `user` ADD COLUMN `medical_examination_expiry_date` date;
ALTER TABLE `user` ADD COLUMN `state` varchar(255);
ALTER TABLE `vacation` ADD CONSTRAINT `vacation_ibfk_1` FOREIGN KEY (`driver_pesel`) REFERENCES `user` (`pesel`);