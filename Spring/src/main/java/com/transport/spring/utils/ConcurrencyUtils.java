package com.transport.spring.utils;


import com.transport.spring.exception.TransportCompanyUnexpectedException;

import java.util.concurrent.Callable;

/**
 * Util do zarządzania konkurencją
 */
public class ConcurrencyUtils {
    /**
     * Tworzy obiekt dbający o to, aby maksymalnie jedna instancja klasy clazz,
     * została stworzona per odwołujący się do niej wątek.
     *
     * <B>NIGDY NIE POWINNO SIĘ PRZECHOWYWAĆ REFERNCJI NA OBIEKT ZWRÓCONY PRZEZ ThreadLocal.get()</B>
     *
     * Aby obiekt ThreadLocal spełniał swoje zadanie powinien być przechowywany najlepiej jako 'final static'
     *
     * @param clazz klasa z konstruktorem bezparametrowym
     * @param <T> typ klasy zwracanej
     *
     * @return obiekt dbający o to, aby maksymalnie jedna instancja klasy clazz,
     *         została stworzona per wątek
     */
    public static <T> ThreadLocal<T> oneInstancePerThread(final Class<T> clazz) {
        return new ThreadLocal<T>() {
            @Override
            protected T initialValue() {
                try {
                    return clazz.newInstance();
                }
                catch (Exception e) {
                    throw new TransportCompanyUnexpectedException(e.getMessage(), e);
                }
            }
        };
    }

    /**
     * Tworzy obiekt dbający o to, aby Callable.call zostało wykonane maksymalnie raz na odwołujący się do niej wątek.
     *
     * <B>NIGDY NIE POWINNO SIĘ PRZECHOWYWAĆ REFERNCJI NA OBIEKT ZWRÓCONY PRZEZ ThreadLocal.get()</B>
     *
     * Aby obiekt ThreadLocal spełniał swoje zadanie powinien być przechowywany najlepiej jako 'final static'
     *
     * @param callable obiekt który ma zostać uruchomiony maksymalnie raz per wątek
     * @param <T> typ klasy zwracanej
     *
     * @return obiekt dbający o to, aby Callable.call zostało wykonane maksymalnie raz na odwołujący się do niej wątek
     */
    public static <T> ThreadLocal<T> oneCallPerThread(final Callable<T> callable) {
        return new ThreadLocal<T>() {
            @Override
            protected T initialValue() {
                try {
                    return callable.call();
                }
                catch (Exception e) {
                    throw new TransportCompanyUnexpectedException(e.getMessage(), e);
                }
            }
        };
    }
}
