package com.transport.spring.utils;

import com.transport.spring.enums.Described;

import java.nio.charset.Charset;
import java.util.*;

/**
 * Util do obsługi stringów w systemie
 */
public class StringUtils {
    private static final Charset CHARSET_UTF8 = Charset.forName("UTF-8");

    /**
     * Tablica znaków dostępnych do enkodowania timestamp'a,
     * bez podstawowych homoglifów (homoglyph) - I (1), O (0).
     */
    private final static char[] DIGITS = {//
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',//
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',//
            'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U', 'W', 'X',//
            'Y', 'Z'//
    };

    /**
     * Maksymalna długość wygenerowanego ciągu znaków przez encodeExceptionId(),
     * dla zadanego słownika DIGITS
     */
    private static final int MAX_LENGTH = 14;

    private static final char[] BASE_CHAR_ARRAY;

    static {
        BASE_CHAR_ARRAY = new char[MAX_LENGTH];
        Arrays.fill(BASE_CHAR_ARRAY, DIGITS[0]);
    }

    public static final String bannedFilenameSignsRegex = "[;*=*:*/*^*<*\\\\*>*?*,*]";

    /**
     * @param s String do sprawdzenia, czy jest pusty
     *
     * @return true, jeśli podany argument jest null'em, pustym stringiem lub zawiera tylko białe znaki
     */
    public static boolean isEmpty(String s) {
        return trim(s) == null;
    }

    /**
     * @param s String który ma zostać pozbawiony białych znaków z początku i końca
     *
     * @return String pozbawiony białych znaków
     */
    public static String trim(String s) {
        if (s == null) {
            return null;
        }

        String trimed = s.trim();
        if (trimed.length() == 0) {
            return null;
        }

        return trimed;
    }

    /**
     * Zwraca obiekt jako String
     * Jeżeli obiekt jest nullem to zwracany jest null
     * Jeżeli obiekt jest pustym (po trimie) stringiem to zwracany jest null
     * Jeżeli obiekt jest niepustym stringiem to ten obiekt jest zwracany po wykonaniu trima
     * Jeżeli obiekt jest innego typu niż string to zwracane jest o.toString()
     *
     * @param o obiekt do ostringowania :)
     *
     * @return wartość string lub null
     */
    public static String toString(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof String) {
            String str = ((String) o).trim();
            if (str.isEmpty()) {
                return null;
            }
            return str;
        }
        return o.toString();
    }

    /**
     * połącz listę łańcuchów tekstowych za pomocą np
     * StringUtils.join({"Ala", "kota", "też psa"}, " ma ") zwróci "Ala ma kota, ma też psa"
     *
     * @param glue string który będzie łączył listę
     * @param list lista stringów
     *
     * @return String
     */
    public static String join(String glue, Collection<String> list) {
        return org.springframework.util.StringUtils.collectionToDelimitedString(list, glue);
    }

    /**
     * połącz listę łańcuchów tekstowych za pomocą np
     * StringUtils.join({"Ala", "kota", "też psa"}, " ma ") zwróci "Ala ma kota, ma też psa"
     *
     * @param glue string który łączy listę
     * @param glued polaczony string
     *
     * @return String
     */
    public static String[] split(String glue, String glued) {
        if (glued == null) {
            return new String[0];
        }

        return glued.split(glue);
    }

    /**
     * połącz tablice łańcuchów tekstowych za pomocą np
     * StringUtils.join({"Ala", "kota", "też psa"}, " ma ") zwróci "Ala ma kota, ma też psa"
     *
     * @param glue string który będzie łączył tablicę
     * @param array array stringów
     *
     * @return String
     */
    public static String join(String glue, String... array) {
        return org.springframework.util.StringUtils.arrayToDelimitedString(array, glue);
    }

    public static Charset getDefaultCharset() {
        return getUtf8Charset();
    }

    public static Charset getUtf8Charset() {
        return CHARSET_UTF8;
    }

    /**
     * Konwertuję podaną liczbę do odpowiadającego jej ciągu znaków (w sposób odwracalny)
     * (lekko zmodyfikowany dla czytelności wynikowego kodu algorytm Base32, z pominięciem znaków I i O - homoglifów)
     *
     * @param errorId liczba do konwersji
     *
     * @return liczba po konwersji
     */
    public static String encodeErrorId(long errorId) {
        final boolean negative = (errorId < 0);
        if (!negative) {
            errorId = -errorId;
        }

        final int digitsLength = DIGITS.length;
        final char[] buffer = BASE_CHAR_ARRAY.clone();

        int index = MAX_LENGTH - 1;
        while (errorId <= -digitsLength) {
            buffer[index--] = DIGITS[(int) (-(errorId % digitsLength))];
            errorId /= digitsLength;
        }

        buffer[index] = DIGITS[(int) (-errorId)];
        if (negative) {
            buffer[--index] = '-';
        }

        return new String(buffer, index, (MAX_LENGTH - index));
    }


    /**
     * Metoda zwraca pusty string <code>""</code> w przypadku gdy podana wartość jest <b>null</b>-em, w innym przypadku przekazana
     * wartość
     *
     * @param string string który może być null-em
     *
     * @return bezpieczna reprezentacja stringa
     */
    public static String safeNull(String string) {
        return isEmpty(string) ? "" : string;
    }

    /**
     * Pobierz wartość tekstową dla podanej zmiennej/wartości
     *
     * @param value wartość, którą chcemy zamienić na string
     *
     * @return string który najbardziej pasuje do opisu
     */
    public static String getStringValue(Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof Enum<?>) {
            // zapisz nazwę by można było ją odczytać Enum#valueOf
            return ((Enum<?>) value).name();
        }
        if (value instanceof Described) {
            return ((Described) value).getHumanReadableDescription();
        }
        if (value instanceof Date) {
            // zapisz w naszym formacie
            return DateUtils.getDateAsString((Date) value);
        }
        if (value instanceof Collection<?>) {
            Collection<?> collection = (Collection) value;
            if (collection.isEmpty()) {
                return "";
            }
            List<String> values = new LinkedList<>();
            for (Object o : collection) {
                values.add(getStringValue(o));
            }
            return StringUtils.join(", ", values);
        }
        return value.toString().trim();
    }

    /**
     * Procesor (builder) dla stringów. Z jego pomocą możemy wykonać na stringach różne operacje, jak na przykład zamienić polskie
     * litery na ich łacińskie odpowiedniki lub usunąć wszystkie znaki niebędące znakami ASCII.
     */
    public static class StringProcessor {
        private static final Map<Character, Character> plDiactToNonDiactMap;

        static {
            plDiactToNonDiactMap = new LinkedHashMap<>(19, 1.0f);
            // ą
            plDiactToNonDiactMap.put('ą', 'a');
            plDiactToNonDiactMap.put('Ą', 'A');
            // ć
            plDiactToNonDiactMap.put('ć', 'c');
            plDiactToNonDiactMap.put('Ć', 'C');
            // ę
            plDiactToNonDiactMap.put('ę', 'e');
            plDiactToNonDiactMap.put('Ę', 'E');
            // ł
            plDiactToNonDiactMap.put('ł', 'l');
            plDiactToNonDiactMap.put('Ł', 'L');
            // ń
            plDiactToNonDiactMap.put('ń', 'n');
            plDiactToNonDiactMap.put('Ń', 'N');
            // ó
            plDiactToNonDiactMap.put('ó', 'o');
            plDiactToNonDiactMap.put('Ó', 'O');
            // ś
            plDiactToNonDiactMap.put('ś', 's');
            plDiactToNonDiactMap.put('Ś', 'S');
            // ź
            plDiactToNonDiactMap.put('ź', 'z');
            plDiactToNonDiactMap.put('Ź', 'Z');
            // ż
            plDiactToNonDiactMap.put('ż', 'z');
            plDiactToNonDiactMap.put('Ż', 'Z');
        }

        private char[] chars;

        /**
         * Konstruktor przyjmujący string, na którym chcemy wykonać operację z wykorzystaniem tego procesora
         *
         * @param inputString string
         */
        public StringProcessor(String inputString) {
            this.chars = inputString.toCharArray();
        }

        /**
         * Usuwa polskie znaki diaktryczne co w efekcie zamienia wszystkie polskie litery na ich ogólne odpowiedniki łacińskie.
         *
         * @return ten procesor
         */
        public StringProcessor removePolishDiactricalMarks() {
            for (int i = 0; i < chars.length; i++) {
                chars[i] = plDiactToNonDiactMap.containsKey(chars[i]) ? plDiactToNonDiactMap.get(chars[i]) : chars[i];
            }
            return this;
        }

        /**
         * Zamienia wszystki znaki nie-ASCII znakiem przekazanym w parametrze
         *
         * @param charToBeReplacedWith znak, jakim zamienić wszystkie przekazane znaki
         *
         * @return ten procesor
         */
        public StringProcessor replaceNonAsciiCharsWith(char charToBeReplacedWith) {
            assert charToBeReplacedWith <= 127 : "Przekazany znak nie jest znakiem ASCII";
            byte[] asciiBytes = new byte[chars.length];
            for (int i = 0; i < chars.length; i++) {
                asciiBytes[i] = (byte) (chars[i] > 127 ? charToBeReplacedWith : chars[i]);
            }
            this.chars = new String(asciiBytes, Charset.forName("US-ASCII")).toCharArray();
            return this;
        }

        /**
         * @return nowy string po dokonanych przekształceniach
         */
        public String buildString() {
            return new String(chars);
        }
    }

    /**
     * Zwraca reprezentację tekstową wartości boolowskiej
     *
     * @param bool
     *
     * @return jeśli <code>Boolean.TRUE<code/> zwraca "TAK" w przeciwnym wypadku "NIE"
     */
    public static String getBooleanAsString(Boolean bool) {
        return Boolean.TRUE.equals(bool) ? "TAK" : "NIE";
    }

    /**
     * Zwróć pierwszy niepusty string.
     *
     * @param strings kolekcja stringów.
     *
     * @return pierwszy niepusty argument wywołania lub pusty napis.
     */
    public static String coalesce(final String... strings) {
        for (String string : strings) {
            if (!isEmpty(string)) {
                return string;
            }
        }
        return "";
    }
}
