package com.transport.spring.utils;

import com.transport.spring.exception.InvalidArgumentException;
import com.transport.spring.exception.TransportCompanyUnexpectedException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.Callable;

/**
 * Util do obsługi dat w systemie
 */
public class DateUtils {
    /**
     * Format daty
     */
    public static final String DATE_FORMAT = "dd/MM/yyyy";

    /**
     * Format daty dla raportu,
     * <b>używane bezpośrednio w raportach</b>
     */
    @SuppressWarnings("UnusedDeclaration")
    public static final String REPORT_DATE_FORMAT = "dd.MM.yyyy";

    /**
     * Jeden SDF zostanie stworzony dla każdego wątku próbującego sformatować datę chociaż raz,
     * rozwiązuje problem konkurencji z SDF jednocześnie nie tworząc nowego SDF dla każdej operacji formatowania daty.
     *
     * Ogranicza zużycie pamięci i procesora.
     */
    private static final ThreadLocal<DateFormat> LOCAL_DATE_FORMATTER =
            ConcurrencyUtils.oneCallPerThread(new DateFormatCallableFactory(DATE_FORMAT));

    /**
     * Format daty z czasem
     */
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";

    /**
     * Format data zgodny z ISO8601
     */
    public static final String DATE_TIME_ISO8601 = "yyyy-MM-dd'T'HH:mm:ssZ";

    /**
     * Jeden SDF zostanie stworzony dla każdego wątku próbującego sformatować datę chociaż raz,
     * rozwiązuje problem konkurencji z SDF jednocześnie nie tworząc nowego SDF dla każdej operacji formatowania daty.
     *
     * Ogranicza zużycie pamięci i procesora.
     */
    private static final ThreadLocal<DateFormat> LOCAL_DATE_TIME_FORMATTER =
            ConcurrencyUtils.oneCallPerThread(new DateFormatCallableFactory(DATE_TIME_FORMAT));

    /**
     * @param dateString string z datą
     *
     * @return obiekt daty na podstawie stringa
     *
     * @throwsInvalidArgumentException jeżeli data nie jest zgodna z naszym formatem
     */
    public static Date parseDate(String dateString) throws InvalidArgumentException {
        return parseDateUsingFormatter(dateString, LOCAL_DATE_FORMATTER);
    }

    /**
     * @param dateTimeString string z datą i czasem
     *
     * @return obiekt daty na podstawie stringa
     *
     * @throws InvalidArgumentException jeżeli data nie jest zgodna z naszym formatem
     */
    public static Date parseDateTime(String dateTimeString) throws InvalidArgumentException {
        return parseDateUsingFormatter(dateTimeString, LOCAL_DATE_TIME_FORMATTER);
    }

    /**
     * Używa formatera do konwersji ciągu znaków do obiektu Date
     *
     * @param dateString data jako String, lub null
     * @param localDateFormatter formatter
     *
     * @return data, lub null
     *
     * @throws InvalidArgumentException jeżeli nie udało się skonwertować ciągu znaków do obiektu Date
     */
    private static Date parseDateUsingFormatter(String dateString, ThreadLocal<DateFormat> localDateFormatter)
            throws InvalidArgumentException {
        if (StringUtils.isEmpty(dateString)) {
            return null;
        }

        try {
            return localDateFormatter.get().parse(dateString);
        }
        catch (ParseException e) {
            throw new InvalidArgumentException(e.getMessage(), e);
        }
    }

    /**
     * @param date obiekt daty
     *
     * @return data jako String
     */
    public static String getDateAsString(Date date) {
        if (date == null) {
            return null;
        }
        return LOCAL_DATE_FORMATTER.get().format(date);
    }

    /**
     * Przycina datę do początku dnia
     * @param date data
     * @return data przycięta
     */
    public static Date trimToDayStart(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Przycina datę do początku roku
     * @param date data
     * @return data przycięta
     */
    public static Date trimToYearStart(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        return calendar.getTime();
    }

    /**
     * Przycina datę do początku roku
     * @param date data
     * @return data przycięta
     */
    public static Date trimToYearEnd(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        return calendar.getTime();
    }

    /**
     * Zaokrągla datę do końca dnia
     * @param date data
     * @return zaokrąglona
     */
    public static Date trimToDayEnd(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * Zwróc string z data i czasem
     *
     * @param date data do sformatowania
     *
     * @return sformatowana data z czasem
     */
    public static String getDateWithTimeAsString(Date date) {
        if (date == null) {
            return null;
        }
        return LOCAL_DATE_TIME_FORMATTER.get().format(date);
    }

    /**
     * Zwraca datę w odpowiednim formacie
     *
     * <b><br/>NIE POWINNO SIĘ TEJ METODY UŻYWAĆ DLA FORMATÓW DateUtils.DATE_FORMAT i DateUtils.DATE_TIME_FORMAT,
     * dla tych formatów istnieją optymalniejsze metody</b>
     *
     * @param date data
     * @param dateFormat format zgodny z {@link SimpleDateFormat}
     *
     * @return data w odpowiednim formacie
     */
    public static String getDateAsStringInFormat(Date date, String dateFormat) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat).format(date);
    }

    /**
     * Zwraca datę w formacie {@link XMLGregorianCalendar}
     *
     * @param date data do konwersji
     *
     * @return data w żądanym formacie
     */
    public static XMLGregorianCalendar getDateAsXmlGregorianCalendar(Date date) {
        GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
        gregorianCalendar.setTime(date);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        }
        catch (DatatypeConfigurationException e) {
            throw new TransportCompanyUnexpectedException(e.getMessage(), e);
        }
    }

    /**
     * Pobierz rok z daty.
     *
     * @param date przekazana data - może byc <code>null</code>.
     *
     * @return pusty String lub rok w postaci napisu.
     */
    public static String getYearFromDate(final Date date) {
        if (date == null) {
            return "";
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return Integer.toString(calendar.get(Calendar.YEAR));
    }

    /**
     * Factory DateFormat w zadanym formacie
     */
    private static class DateFormatCallableFactory implements Callable<DateFormat> {
        public final String dateFormat;

        private DateFormatCallableFactory(String dateFormat) {
            this.dateFormat = dateFormat;
        }

        @Override
        public DateFormat call() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            simpleDateFormat.setLenient(false);

            return simpleDateFormat;
        }
    }

    /**
     * Pozwala na sprawdzenie, czy podane daty występują w podanym porządku
     */
    public static class DateOrder {

        /**
         * Sprawdz czy podane daty występują w podanej kolejności (dopuszczamy równość dat)
         *
         * @param earlierDate wcześniejsza data
         * @param laterDate późniejsza data
         *
         * @return <code>true</code> jezeli zachowana jest kolejność dat
         */
        public static boolean isInGrowingOrderDate(Date earlierDate, Date laterDate) {
            // jeżeli dwa null-e leci nullpointer z before
            if (earlierDate == null && laterDate == null) {
                return false;
            }

            // dla czytelności nie upraszczamy
            if (earlierDate == null || !(earlierDate.before(laterDate) || earlierDate.equals(laterDate))) {
                return false;
            }

            //noinspection RedundantIfStatement
            if (laterDate == null || !(laterDate.after(earlierDate) || laterDate.equals(earlierDate))) {
                return false;
            }

            return true;
        }

        /**
         * Sprawdz czy podane daty występują w podanej kolejności. Daty w formie stringa konwertowane są do postacji Daty
         * <b>zawierającej czas</b>
         *
         * @param earlierDate wcześniejsza data
         * @param laterDate późniejsza data
         *
         * @return <code>true</code> jezeli zachowana jest kolejność dat
         *
         * @throws InvalidArgumentException błędy parsowania
         */
        public static boolean isInGrowingOrderDateTime(String earlierDate, String laterDate) throws InvalidArgumentException {
            Date earlier = null;
            Date later = null;
            if (earlierDate != null) {
                earlier = parseDateTime(earlierDate);
            }
            if (laterDate != null) {
                later = parseDateTime(laterDate);
            }
            return isInGrowingOrderDate(earlier, later);
        }

        /**
         * Sprawdz czy podane daty występują w podanej kolejności. Daty w formie stringa konwertowane są do postacji Daty <b>bez
         * czasu</b>
         *
         * @param earlierDate wcześniejsza data
         * @param laterDate późniejsza data
         *
         * @return <code>true</code> jezeli zachowana jest kolejność dat
         *
         * @throws InvalidArgumentException błędy parsowania
         */
        public static boolean isInGrowingOrderDate(String earlierDate, String laterDate) throws InvalidArgumentException {
            Date earlier = null;
            Date later = null;
            if (earlierDate != null) {
                earlier = parseDate(earlierDate);
            }
            if (laterDate != null) {
                later = parseDate(laterDate);
            }
            return isInGrowingOrderDate(earlier, later);
        }
    }
}
