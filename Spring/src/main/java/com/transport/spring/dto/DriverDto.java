package com.transport.spring.dto;

/**
 * Dto dla Kierowcy
 */

import com.transport.spring.enums.DriverStateEnum;
import com.transport.spring.enums.UserTypeEnum;

import java.util.Date;

public class DriverDto extends UserDto {
    private Date drivingLicenceExpiryDate;
    private Date medicalExaminationExpiryDate;
    private DriverStateEnum state;
    private Long vacationId;

    public DriverDto(){
    }

    public DriverDto(String pesel, String login, String password, String name, String surname, String email, UserTypeEnum type, Date drivingLicenceExpiryDate,
                     Date medicalExaminationExpiryDate, String idNumber, DriverStateEnum state, Long vacationId){
        super(pesel, login, password, name, surname, email, type, idNumber);
        this.drivingLicenceExpiryDate = drivingLicenceExpiryDate;
        this.medicalExaminationExpiryDate = medicalExaminationExpiryDate;
        this.state = state;
        this.vacationId = vacationId;
    }

    public DriverDto(String pesel, String login, String password, String name, String surname, String email, UserTypeEnum type, Date drivingLicenceExpiryDate,
                     Date medicalExaminationExpiryDate, String idNumber, DriverStateEnum state){
        super(pesel, login, password, name, surname, email, type, idNumber);
        this.drivingLicenceExpiryDate = drivingLicenceExpiryDate;
        this.medicalExaminationExpiryDate = medicalExaminationExpiryDate;
        this.state = state;
    }

    public Date getDrivingLicenceExpiryDate() {
        return drivingLicenceExpiryDate;
    }

    public void setDrivingLicenceExpiryDate(Date drivingLicenceExpiryDate) {
        this.drivingLicenceExpiryDate = drivingLicenceExpiryDate;
    }

    public Date getMedicalExaminationExpiryDate() {
        return medicalExaminationExpiryDate;
    }

    public void setMedicalExaminationExpiryDate(Date medicalExaminationExpiryDate) {
        this.medicalExaminationExpiryDate = medicalExaminationExpiryDate;
    }

    public DriverStateEnum getState() {
        return state;
    }

    public void setState(DriverStateEnum state) {
        this.state = state;
    }

    public Long getVacationId() {
        return vacationId;
    }

    public void setVacationId(Long vacationId) {
        this.vacationId = vacationId;
    }
}
