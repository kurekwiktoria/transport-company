package com.transport.spring.dto;

/**
 * Dto dla Naprawy Pojazdu
 */

import java.util.Date;

public class RepairDto {
    private Long id;
    private Long cost;
    private String description;
    private Date dateFrom;
    private Date dateTo;
    private String vehicleVin;

    public RepairDto(){
    }

    public RepairDto(Long id, Long cost, String description, Date dateFrom, Date dateTo, String vehicleVin) {
        this.id = id;
        this.cost = cost;
        this.description = description;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.vehicleVin = vehicleVin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getVehicleVin() {
        return vehicleVin;
    }

    public void setVehicleVin(String vehicleVin) {
        this.vehicleVin = vehicleVin;
    }
}
