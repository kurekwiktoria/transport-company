package com.transport.spring.dto;

import com.transport.spring.enums.VehicleStateEnum;
import com.transport.spring.enums.VehicleTypeEnum;

import java.util.Date;
import java.util.List;

/**
 * Dto dla Pojazdu
 */

public class VehicleDto {
    private String vin;
    private String brand;
    private String model;
    private Long power;
    private Long tankCapacity;
    private Long combustion;
    private Long vehicleCapacity;
    private Date serviceCheckExpiryDate;
    private Date insuranceExpiryDate;
    private VehicleStateEnum state;
    private Long mileage;
    private VehicleTypeEnum type;
    private String registrationNumber;
    private List<RepairDto> repairDtos;
    private List<TransportDto> transportDtos;
    private String firmNip;


    public VehicleDto() {
    }

    public VehicleDto(String vin, String brand, String model, Long power, Long tankCapacity, Long combustion, Long vehicleCapacity, Date serviceCheckExpiryDate,
                      Date insuranceExpiryDate, VehicleStateEnum state, Long mileage, VehicleTypeEnum type, String registrationNumber) {
        this.vin = vin;
        this.brand = brand;
        this.model = model;
        this.power = power;
        this.tankCapacity = tankCapacity;
        this.combustion = combustion;
        this.vehicleCapacity = vehicleCapacity;
        this.serviceCheckExpiryDate = serviceCheckExpiryDate;
        this.insuranceExpiryDate = insuranceExpiryDate;
        this.state = state;
        this.mileage = mileage;
        this.type = type;
        this.registrationNumber = registrationNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    public Long getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(Long tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public Long getCombustion() {
        return combustion;
    }

    public void setCombustion(Long combustion) {
        this.combustion = combustion;
    }

    public Long getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(Long vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public Date getServiceCheckExpiryDate() {
        return serviceCheckExpiryDate;
    }

    public void setServiceCheckExpiryDate(Date serviceCheckExpiryDate) {
        this.serviceCheckExpiryDate = serviceCheckExpiryDate;
    }

    public Date getInsuranceExpiryDate() {
        return insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(Date insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    public VehicleStateEnum getState() {
        return state;
    }

    public void setState(VehicleStateEnum state) {
        this.state = state;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public List<RepairDto> getRepairDtos() {
        return repairDtos;
    }

    public void setRepairDtos(List<RepairDto> repairDtos) {
        this.repairDtos = repairDtos;
    }

    public List<TransportDto> getTransportDtos() {
        return transportDtos;
    }

    public void setTransportDtos(List<TransportDto> transportDtos) {
        this.transportDtos = transportDtos;
    }

    public String getFirmNip() {
        return firmNip;
    }

    public void setFirmNip(String firmNip) {
        this.firmNip = firmNip;
    }
}
