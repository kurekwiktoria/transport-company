package com.transport.spring.dto;

import java.util.Date;

/**
 * Dto dla Urlopu
 */

public class VacationDto {
    private Long id;
    private Date dateFrom;
    private Date dateTo;
    private String reason;
    private String driverPesel;

    public VacationDto(){}

    public VacationDto(Long id, Date dateFrom, Date dateTo, String reason, String driverPesel) {
        this.id = id;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.reason = reason;
        this.driverPesel = driverPesel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDriverPesel() {
        return driverPesel;
    }

    public void setDriverPesel(String driverPesel) {
        this.driverPesel = driverPesel;
    }
}
