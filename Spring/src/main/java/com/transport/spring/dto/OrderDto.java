package com.transport.spring.dto;

import java.util.Date;

/**
 * Dto dla Zlecenia tworzonego przez Zleceniodawcę
 */
public class OrderDto {
    private Long id;
    private String loadingPlace;
    private String destination;
    private Date loadingDate;
    private Date unloadingDate;
    private Boolean isFinished;
    private Long distance;
    private Float price;
    private String description;
    private String userPesel;
    private Long transportId;

    public OrderDto(){}

    public OrderDto(Long id, String loadingPlace, String destination, Date loadingDate, Date unloadingDate,
                    Boolean isFinished, Long distance, Float price, String description) {
        this.id = id;
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.loadingDate = loadingDate;
        this.unloadingDate = unloadingDate;
        this.isFinished = isFinished;
        this.distance = distance;
        this.price = price;
        this.description = description;
    }

    public OrderDto(Long id, String loadingPlace, String destination, Date loadingDate, Date unloadingDate,
                    Boolean isFinished, Long distance, Float price, String description, String userPesel) {
        this.id = id;
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.loadingDate = loadingDate;
        this.unloadingDate = unloadingDate;
        this.isFinished = isFinished;
        this.distance = distance;
        this.price = price;
        this.description = description;
        this.userPesel = userPesel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoadingPlace() {
        return loadingPlace;
    }

    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(Date loadingDate) {
        this.loadingDate = loadingDate;
    }

    public Date getUnloadingDate() {
        return unloadingDate;
    }

    public void setUnloadingDate(Date unloadingDate) {
        this.unloadingDate = unloadingDate;
    }

    public Boolean getFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserPesel() {
        return userPesel;
    }

    public void setUserPesel(String userPesel) {
        this.userPesel = userPesel;
    }

    public Long getTransportId() {
        return transportId;
    }

    public void setTransportId(Long transportId) {
        this.transportId = transportId;
    }
}
