package com.transport.spring.dto;

/**
 * Dto dla Przewozu tworzonego ze Zlecenia po zaakceptowaniu przez Spedytora
 */
public class TransportDto {
    private Long id;
    private Long burnedFuel;
    private Long driverSalary;
    private Long profit;
    private Boolean isFinished;
    private Long orderId;
    private String vehicleVin;

    public TransportDto(){}

    public TransportDto(Long id, Long burnedFuel, Long driverSalary, Long profit, Boolean isFinished) {
        this.id = id;
        this.burnedFuel = burnedFuel;
        this.driverSalary = driverSalary;
        this.profit = profit;
        this.isFinished = isFinished;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBurnedFuel() {
        return burnedFuel;
    }

    public void setBurnedFuel(Long burnedFuel) {
        this.burnedFuel = burnedFuel;
    }

    public Long getDriverSalary() {
        return driverSalary;
    }

    public void setDriverSalary(Long driverSalary) {
        this.driverSalary = driverSalary;
    }

    public Long getProfit() {
        return profit;
    }

    public void setProfit(Long profit) {
        this.profit = profit;
    }

    public Boolean getFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getVehicleVin() {
        return vehicleVin;
    }

    public void setVehicleVin(String vehicleVin) {
        this.vehicleVin = vehicleVin;
    }
}
