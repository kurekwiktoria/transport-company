package com.transport.spring.dto;

/**
 * Dto dla Użytkownika
 */

import com.transport.spring.enums.UserTypeEnum;

import java.util.List;

public class UserDto {
    private String pesel;
    private String login;
    private String password;
    private String name;
    private String surname;
    private String email;
    private UserTypeEnum type;
    private String idNumber;
    private List<OrderDto> orderDtos;
    private String firmNip;


    public UserDto(){
    }

    public UserDto(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public UserDto(String pesel, String login, String password, String name, String surname, String email, UserTypeEnum type, String idNumber){
        this.pesel = pesel;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.type = type;
        this.idNumber = idNumber;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserTypeEnum getType() {
        return type;
    }

    public void setType(UserTypeEnum type) {
        this.type = type;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<OrderDto> getOrderDtos() {
        return orderDtos;
    }

    public void setOrderDtos(List<OrderDto> orderDtos) {
        this.orderDtos = orderDtos;
    }

    public String getFirmNip() {
        return firmNip;
    }

    public void setFirmNip(String firmNip) {
        this.firmNip = firmNip;
    }
}
