package com.transport.spring.dto;

import java.util.List;

/**
 * Dto dla Firmy, którą posiada spedytor
 */
public class FirmDto {
    private String nip;
    private String name;
    private String city;
    private String street;
    private Long streetNumber;
    private Long houseNumber;
    private String zipCode;
    private String phone;
    private String fax;
    private String regon;
    private String email;
    private String krs;
    private String userPesel;
    private List<VehicleDto> vehicleDtos;

    public FirmDto(){
    }

    public FirmDto(String nip, String name, String city, String street, Long streetNumber, Long houseNumber,
                   String zipCode, String phone, String fax, String regon, String email, String krs) {
        this.nip = nip;
        this.name = name;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
        this.houseNumber = houseNumber;
        this.zipCode = zipCode;
        this.phone = phone;
        this.fax = fax;
        this.regon = regon;
        this.email = email;
        this.krs = krs;
    }
    public FirmDto(String nip, String name, String city, String street, Long streetNumber, Long houseNumber,
                   String zipCode, String phone, String fax, String regon, String email, String krs, String userId) {
        this.nip = nip;
        this.name = name;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
        this.houseNumber = houseNumber;
        this.zipCode = zipCode;
        this.phone = phone;
        this.fax = fax;
        this.regon = regon;
        this.email = email;
        this.krs = krs;
        this.userPesel = userId;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Long streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Long getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Long houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKrs() {
        return krs;
    }

    public void setKrs(String krs) {
        this.krs = krs;
    }

    public String getUserPesel() {
        return userPesel;
    }

    public void setUserPesel(String userPesel) {
        this.userPesel = userPesel;
    }

    public List<VehicleDto> getVehicles() {
        return vehicleDtos;
    }

    public void setVehicles(List<VehicleDto> vehicles) {
        this.vehicleDtos = vehicles;
    }
}
