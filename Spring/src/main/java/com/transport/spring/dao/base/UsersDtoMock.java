package com.transport.spring.dao.base;

import com.transport.spring.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

public class UsersDtoMock {
        public static List<UserDto> getUsers() {
            List<UserDto> clients = new ArrayList<>();
            clients.add(new UserDto("Jason", "Best", "asdf@wp.pl"));
            clients.add(new UserDto("Garry", "Moore", "asdf@wp.pl"));
            clients.add(new UserDto("Marshall", "Mathers", "asdf@wp.pl"));
            clients.add(new UserDto("Paweł", "Domagała", "asdf@wp.pl"));
            clients.add(new UserDto("Dawid", "Podsiadło", "asdf@wp.pl"));
            clients.add(new UserDto("Kuba", "Wojewódzki", "asdf@wp.pl"));
            clients.add(new UserDto("Marcin", "Dorociński", "asdf@wp.pl"));
            clients.add(new UserDto("Krzysztof", "Zalewski", "asdf@wp.pl"));
            clients.add(new UserDto("Maciej", "Maleńczuk", "asdf@wp.pl"));
            clients.add(new UserDto("Milky", "Chance", "asdf@wp.pl"));

            return clients;
        }
    }
