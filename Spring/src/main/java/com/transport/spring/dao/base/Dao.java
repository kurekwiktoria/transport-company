package com.transport.spring.dao.base;

import com.transport.spring.entity.base.Persistable;
import com.transport.spring.exception.EntityNotFoundException;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Interfejs DAO dla obiektów Persistable
 *
 * @param <T> klasa Persistable
 */
@SuppressWarnings("unused")
public interface Dao<T extends Persistable> {
    /**
     * Odłącza obiekt od kontekstu / sesji Hibernate przenosząc go w stan Transient
     *
     * @param entity do odłączenia
     */
    void detach(T entity);

    /**
     * Synchronizuje stan sesji Hibernate z bazą danych (uruchamia polecenia SQL w aktualnej transakcji)
     */
    void flush();

    /**
     * Tworzy nowy wpis w bazie i przenosi encję w stan Managed
     *
     * @param entity do zapisania
     */
    void persist(T entity);

    void saveOrUpdate(T entity);

    /**
     * Usuwa encję z bazy danych
     *
     * @param entity do usunięcia
     */
    void remove(T entity);

    /**
     * "Podłącza" odłączoną encję do aktualnego kontekstu / sesji Hibernate. Zwraca encję zarządzaną, nie modyfikuje stanu
     * obiektu przekazanego w parametrze
     *
     * @param entity do podłączenia
     *
     * @return zarządzana kopia encji
     */
    T merge(T entity);

    /**
     * Czyści aktualną sesję Hibernate ze wszystkich zarządzanych obiektów (jak detach na każdym obiekcie z sesji)
     */
    void clear();

    /**
     * Pobiera encję po ID
     *
     * @param id Identyfikator bazodanowy
     *
     * @return znaleziona encja lub null jeżeli takiej nie ma
     */
    T find(Serializable id);

    /**
     * Pobiera encję po ID lub rzuca wyjątkiem, jeśli takiej encji dla danego ID nie ma.
     *
     * @param id identyfikator bazodanowy
     *
     * @return znaleziona encja
     *
     * @throws EntityNotFoundException
     */
    T get(Serializable id) throws EntityNotFoundException;

    /**
     * Pobiera listę obiektów dla na podstawie listy id
     *
     * @param ids lista id
     *
     * @return lista obiektów
     *
     * @throws EntityNotFoundException jeśli nie odnaleziono któregoś z obiektów
     */
    List<T> get(Collection<Long> ids) throws EntityNotFoundException;

    /**
     * @return wszystkie encje danego typu posortowane po ID
     */
    List<T> getAll();

}
