package com.transport.spring.controller;

import com.transport.spring.entity.ClientEntity;
import com.transport.spring.entity.DriverEntity;
import com.transport.spring.enums.DriverStateEnum;
import com.transport.spring.exception.EntityNotFoundException;
import com.transport.spring.repository.DriverRepository;
import com.transport.spring.repository.FirmRepository;
import com.transport.spring.repository.UserRepository;
import com.transport.spring.entity.ForwarderEntity;
import com.transport.spring.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {


    @Autowired
    UserRepository userRepository;

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    FirmRepository firmRepository;

    @PostMapping("/api/addUser")
    public ResponseEntity<UserEntity> addUser(@RequestBody UserEntity user) {

        try {
            userRepository.save(user);
        }catch (Exception e){
            System.out.println("ERROR user add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping("/api/addForwarder")
    public ResponseEntity<UserEntity> addForwarder(@RequestBody ForwarderEntity user) {
        try {
            userRepository.save(user);
        }catch (Exception e){
            System.out.println("ERROR user add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping("/api/addDriver")
    public ResponseEntity<UserEntity> addDriver(@RequestBody DriverEntity driver) {

        try {
            userRepository.save(driver);
        }catch (Exception e){
            System.out.println("ERROR driver add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(driver);
    }

    @PostMapping("/api/addClient")
    public ResponseEntity<UserEntity> addClient(@RequestBody ClientEntity client) {

        try {
            userRepository.save(client);
        }catch (Exception e){
            System.out.println("ERROR driver add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(client);
    }

    @RequestMapping("/api/getAll")
    public List<UserEntity> getAll(@RequestParam Integer pageNo, @RequestParam Integer pageSize) {
        PageRequest page = PageRequest.of(pageNo, pageSize);
        List<UserEntity> content = userRepository.findAll(page).getContent();
        return content;
    }

    @RequestMapping("/api/getAllCount")
    public Long getAllCount() {
        return userRepository.count();
    }

    @RequestMapping("/api/getAllDrivers")
    public List<DriverEntity> getAllDrivers(){
        return driverRepository.findAll();
    }

    @RequestMapping("/api/getAllDriversWithoutFirm")
    public List<DriverEntity> getAllDriversWithoutFirm(){
        return driverRepository.findDriverEntitiesByFirmIsNull();
    }

    @RequestMapping("/api/getAllDriversByFirm/{nip}")
    public List<DriverEntity> getAllDriversByFirm(@PathVariable String nip){
        return driverRepository.findDriverEntitiesByFirmNip(nip);
    }

    @RequestMapping("/api/getDriverByPesel/{pesel}")
    public DriverEntity getDriverByPesel(@PathVariable String pesel){
        return driverRepository.findDriverEntityByPesel(pesel);
    }

    @RequestMapping("/api/getAllDriversByState/{state}")
    public List<DriverEntity> getAllDriversByState(@PathVariable DriverStateEnum state){
        if (state instanceof DriverStateEnum)
            return driverRepository.findDriverEntitiesByState(state);
        else
            return null;
    }

    @RequestMapping("/api/getAllDriverOnVacation")
    public List<DriverEntity> getAllDriversOnVacation(){
        return driverRepository.findDriverEntitiesByState(DriverStateEnum.ON_VACATION);
    }


    @RequestMapping("/api/getAllDriversWithExpiredMedicalExamination/")
    public List<DriverEntity> getAllDriversWithExpiredMedicalExamination(){
        return driverRepository.findDriverEntitiesByMedicalExaminationExpiryDateLessThanEqual(new Date());
    }

    @RequestMapping("/api/getAllDriversWithExpiredDrivingLicence/")
    public List<DriverEntity> getAllDriversWithExpiredDrivingLicence(){
        return driverRepository.findDriverEntitiesByDrivingLicenceExpiryDateLessThanEqual(new Date());
    }

    @DeleteMapping("/api/delete/{pesel}")
    public ResponseEntity<UserEntity> delete(@PathVariable String pesel) {
        UserEntity userEntity = userRepository.findUserEntityByPesel(pesel);
        try {
            if ( userEntity != null)
                userRepository.delete(userEntity);
            System.out.println("usuwam " + pesel);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
        return ResponseEntity.ok(userEntity);
    }

    @RequestMapping("/api/makeDriverFired/{pesel}")
    public UserEntity makeDriverFired(@PathVariable String pesel){
        DriverEntity driver = driverRepository.findDriverEntityByPesel(pesel);
        try{
            if(driver != null){
                driver.setFirm(null);
                driver.setState(DriverStateEnum.FIRED);
                driverRepository.save(driver);
            }
        }
        catch (EntityNotFoundException e){
            System.out.println(e.getMessage());
            return null;
        }

        return driver;
    }


    @RequestMapping("/api/makeDriverHired/{pesel}/{nip}")
    public UserEntity makeDriverHired(@PathVariable String pesel, @PathVariable String nip){
        DriverEntity driver = driverRepository.findDriverEntityByPesel(pesel);
        try{
            if(driver != null){
                driver.setFirm(firmRepository.findFirmEntityByNip(nip));
                driver.setState(DriverStateEnum.ACTIVE);
                driverRepository.save(driver);
            }
        }
        catch (EntityNotFoundException e){
            System.out.println(e.getMessage());
            return null;
        }

        return driver;
    }


    @RequestMapping("/api/getUserByLogin/{login}")
    public UserEntity getUserByLogin(@PathVariable String login){
        if (!login.isEmpty())
            return userRepository.findByLogin(login);
        else
            return new UserEntity();
    }

    @RequestMapping("/api/updateUser")
    public void updateUser(@RequestBody String pesel, @RequestBody UserEntity user){
        UserEntity activeUser = userRepository.findUserEntityByPesel(pesel);
        activeUser.setEmail(user.getEmail());
        activeUser.setIdNumber(user.getIdNumber());
        activeUser.setName(user.getName());
        activeUser.setSurname(user.getSurname());
        activeUser.setPassword(user.getPassword());
    }

    @PostMapping("/api/updateDrivingLicence")
    public void updateDrivingLicence(@RequestParam String date, @RequestParam String pesel){
        try {
            DriverEntity activeDriver = driverRepository.findDriverEntityByPesel(pesel);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date newDate = formatter.parse(date);
            System.out.println(date);
            activeDriver.setDrivingLicenceExpiryDate(newDate);
            userRepository.save(activeDriver);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @PostMapping("/api/updateMedicalExamination")
    public void updateMedicalExamination(@RequestParam String date, @RequestParam String pesel){
        try {
            DriverEntity activeDriver = driverRepository.findDriverEntityByPesel(pesel);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date newDate = formatter.parse(date);
            System.out.println(date);
            activeDriver.setMedicalExaminationExpiryDate(newDate);
            userRepository.save(activeDriver);
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
