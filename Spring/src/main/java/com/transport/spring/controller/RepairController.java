package com.transport.spring.controller;

import com.transport.spring.entity.*;
import com.transport.spring.enums.DriverStateEnum;
import com.transport.spring.enums.VehicleStateEnum;
import com.transport.spring.repository.DriverRepository;
import com.transport.spring.repository.RepairRepository;
import com.transport.spring.repository.UserRepository;
import com.transport.spring.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RepairController {


    @Autowired
    RepairRepository repairRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @PostMapping("/api/addRepair")
    public ResponseEntity<RepairEntity> addRepair(@RequestBody RepairEntity repair) {

        try {
            VehicleEntity vehicleEntity = vehicleRepository.findVehicleEntityByVin(repair.getVehicle().getVin());
            repair.setVehicle(vehicleEntity);
            repairRepository.save(repair);
            vehicleEntity.setState(VehicleStateEnum.IN_REPAIR);
            vehicleRepository.save(vehicleEntity);
        }catch (Exception e){
            System.out.println("ERROR repair add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(repair);
    }

    @RequestMapping("/api/getAllRepairs")
    public List<RepairEntity> getAllRepairs() {
        return repairRepository.findAll();
    }

    @RequestMapping("/api/getRepairsByVehicleVin/{vin}")
    public List<RepairEntity> getRepairsByVehicleVin(@PathVariable String vin) {
       return repairRepository.findRepairEntitiesByVehicleVin(vin);
    }

    @RequestMapping("/api/getRepairById/{id}")
    public RepairEntity getRepairById(@PathVariable Long id) {
        return repairRepository.findRepairEntityById(id);
    }

    @RequestMapping("/api/getLastRepair")
    public RepairEntity getLastRepair(){
        Long id = 0L;
        RepairEntity lastRepairEntity = null;
        List<RepairEntity> repairEntities = getAllRepairs();
        for(RepairEntity repairEntity : repairEntities){
            if(repairEntity.getId() > id) {
                id = repairEntity.getId();
                lastRepairEntity = repairEntity;
            }
        }

        return lastRepairEntity;
    }
}
