package com.transport.spring.controller;

import com.transport.spring.entity.FirmEntity;
import com.transport.spring.entity.TransportEntity;
import com.transport.spring.repository.FirmRepository;
import com.transport.spring.repository.TransportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class FirmController {


    @Autowired
    FirmRepository firmRepository;

    @PersistenceContext
    protected EntityManager em;

    @PostMapping("/api/addFirm")
    public ResponseEntity<FirmEntity> addFirm(@RequestBody FirmEntity firm) {

        try {
            firmRepository.save(firm);
        }catch (Exception e){
            System.out.println("ERROR firm add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(firm);
    }

    @RequestMapping("/api/getAllFirms")
    public List<FirmEntity> getAllFirms() {
        return firmRepository.findAll();
    }

    @RequestMapping("/api/getFirmEntityByName/{name}")
    public FirmEntity getFirmEntityByName(@PathVariable String name) {
        return firmRepository.findFirmEntityByName(name);
    }

    @RequestMapping("/api/getFirmEntityByNip/{nip}")
    public FirmEntity getFirmEntityByNip(@PathVariable String nip) {
        return firmRepository.findFirmEntityByNip(nip);
    }

    @PutMapping(value = "/api/updateFirm")
    public void updateFirm(@RequestBody FirmEntity firm){
        FirmEntity activeUserFirm = firmRepository.findFirmEntityByNip(firm.getNip());
        activeUserFirm.setName(firm.getName());
        activeUserFirm.setCity(firm.getCity());
        activeUserFirm.setZipCode(firm.getZipCode());
        activeUserFirm.setStreet(firm.getStreet());
        activeUserFirm.setFax(firm.getFax());
        activeUserFirm.setPhone(firm.getPhone());
        activeUserFirm.setEmail(firm.getEmail());
        firmRepository.save(activeUserFirm);
    }
}
