package com.transport.spring.controller;

import com.transport.spring.Truck;
import com.transport.spring.repository.*;
import com.transport.spring.dao.base.UsersDtoMock;
import com.transport.spring.dto.UserDto;
import com.transport.spring.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class HomeController {

    int getCounter = 0;

    @Autowired
    UserRepository userRepo;

    @RequestMapping("/getTruck")
    public Truck home() {
        Truck truck = new Truck("Volvo", "SX50", 500);
        System.out.println("/getTruck " + getCounter++);
        return truck;
    }

    @RequestMapping("/getUsers")
    public List<UserDto> getUsers() {
        return UsersDtoMock.getUsers();
    }

    @RequestMapping("/getUser")
    public String getUser() {
        return userRepo.findAll().toString();
    }

    @RequestMapping("/insertUser")
    public UserEntity insertUser() {
        UserEntity user = new UserEntity("12312312", "Jurson",
                "hehe", "Juru", "Elo", "jurson@wp.pl", "987");

        userRepo.save(user);

        return user;
    }
}
