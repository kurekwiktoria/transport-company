package com.transport.spring.controller;

import com.transport.spring.entity.DriverEntity;
import com.transport.spring.entity.VacationEntity;
import com.transport.spring.entity.VehicleEntity;
import com.transport.spring.enums.DriverStateEnum;
import com.transport.spring.repository.UserRepository;
import com.transport.spring.repository.VacationRepository;
import com.transport.spring.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class VacationController {


    @Autowired
    VacationRepository vacationRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/api/addVacation")
    public ResponseEntity<VacationEntity> addVacation(@RequestBody VacationEntity vacation) {

        try {
            vacationRepository.save(vacation);
            DriverEntity driverEntity = vacation.getDriver();
            driverEntity.setState(DriverStateEnum.ON_VACATION);
            userRepository.save(driverEntity);

        }catch (Exception e){
            System.out.println("ERROR vacation add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(vacation);
    }

    @RequestMapping("/api/getVacationsByDriverFirmNip/{nip}")
    public List<VacationEntity> getVacationsByDriverFirmNip(@PathVariable String nip) {

        return vacationRepository.findVacationEntitiesByDriverFirmNip(nip);
    }

    @RequestMapping("/api/getAllVacations")
    public List<VacationEntity> getAllVacations() {

        return vacationRepository.findAll();
    }


    @RequestMapping("/api/getLastVacation")
    public VacationEntity getLastVacation(){
        Long id = 0L;
        VacationEntity lastVacation = null;
        List<VacationEntity> vacationEntities = getAllVacations();
        for(VacationEntity vacationEntity : vacationEntities){
            if(vacationEntity.getId() > id) {
                id = vacationEntity.getId();
                lastVacation = vacationEntity;
            }
        }

        return lastVacation;
    }
}
