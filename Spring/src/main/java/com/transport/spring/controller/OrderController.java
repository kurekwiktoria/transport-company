package com.transport.spring.controller;

import com.transport.spring.entity.OrderEntity;
import com.transport.spring.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class OrderController {


    @Autowired
    OrderRepository orderRepository;

    @PostMapping("/api/addOrder")
    public ResponseEntity<OrderEntity> addOrder(@RequestBody OrderEntity order) {

        try {
            order.setAccepted(false);
            orderRepository.save(order);
        }catch (Exception e){
            System.out.println("ERROR order add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(order);
    }

    @RequestMapping("/api/getAllOrders")
    public List<OrderEntity> getAllOrders() {
        return orderRepository.findAll();
    }

    @RequestMapping("/api/getOrderById/{id}")
    public OrderEntity getOrderById(@PathVariable Long id){
        return orderRepository.findOrderEntityById(id);
    }

    @RequestMapping("/api/getAllNotAcceptedOrders")
    public List<OrderEntity> getAllNotAcceptedOrders(){
        return orderRepository.findOrderEntitiesByIsAcceptedIsFalse();
    }

    @RequestMapping("/api/getAllOrdersByUserPesel/{pesel}")
    public List<OrderEntity> getAllOrdersByUserPesel(@PathVariable String pesel){
        return orderRepository.findOrderEntitiesByUserPesel(pesel);
    }

    @RequestMapping("/api/getAllAvailableOrders")
    public List<OrderEntity> getAllAvailableOrders(){
        PageRequest page = PageRequest.of(0, 30);

        return orderRepository.findOrderEntitiesByIsAcceptedIsFalse(page).getContent();
    }

    @RequestMapping("/api/getLastOrder")
    public OrderEntity getLastOrder(){
        Long id = 0L;
        List<OrderEntity> orders = getAllOrders();
        for(OrderEntity order : orders){
            if(order.getId() > id)
                id = order.getId();
        }

        return getOrderById(id);
    }

}
