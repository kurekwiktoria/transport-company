package com.transport.spring.controller;

import com.transport.spring.entity.RepairEntity;
import com.transport.spring.entity.TransportEntity;
import com.transport.spring.repository.RepairRepository;
import com.transport.spring.repository.TransportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TransportController {


    @Autowired
    TransportRepository transportRepository;

    @PostMapping("/api/addTransport")
    public ResponseEntity<TransportEntity> addTransport(@RequestBody TransportEntity transport) {
        transport.setFinished(false);
        transport.getOrder().setAccepted(true);
        transport.setBurnedFuel(transport.getVehicle().getCombustion() * transport.getOrder().getDistance()/100);
        try {
            transportRepository.save(transport);
        }catch (Exception e){
            System.out.println("ERROR transport add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(transport);
    }

    @RequestMapping("/api/getAllTransports")
    public List<TransportEntity> getAllTransports() {
        return transportRepository.findAll();
    }

    @RequestMapping("/api/getAllUnfinishedTransports")
    public List<TransportEntity> getAllUnfinishedTransports() {
       return transportRepository.findTransportEntitiesByIsFinishedFalse();
    }

    @RequestMapping("/api/getAllFinishedTransports")
    public List<TransportEntity> getAllFinishedTransports() {
        return transportRepository.findTransportEntitiesByIsFinishedTrue();
    }

    @RequestMapping("/api/getAllTransportsByFirmNip/{nip}")
    public List<TransportEntity> getAllTransportsByFirmNip(@PathVariable String nip) {
        return transportRepository.findTransportEntitiesByVehicleFirmNip(nip);
    }

    @RequestMapping("/api/getAllTransportsByDriverPesel/{pesel}")
    public List<TransportEntity> getAllTransportsByDriverPesel(@PathVariable String pesel) {
        return transportRepository.findTransportEntitiesByDriverPesel(pesel);
    }

    @RequestMapping("/api/getLastTransport")
    public TransportEntity getLastTransport(){
        Long id = 0L;
        TransportEntity lastTransportEntity = null;
        List<TransportEntity> transportEntities = getAllTransports();
        for(TransportEntity transportEntity : transportEntities){
            if(transportEntity.getId() > id) {
                id = transportEntity.getId();
                lastTransportEntity = transportEntity;
            }
        }

        return lastTransportEntity;
    }

    @RequestMapping("/api/getFutureTransportsByDriverPesel/{pesel}")
    public List<TransportEntity> getFutureTransportsByDriverPesel(@PathVariable String pesel) {
        List<TransportEntity> transportEntities = transportRepository.findTransportEntitiesByDriverPesel(pesel);
        List<TransportEntity> futureTransports = new ArrayList<>();
        for(TransportEntity transportEntity: transportEntities){
            if(transportEntity.getOrder().getLoadingDate().after(new Date()))
                futureTransports.add(transportEntity);
        }

        return futureTransports;
    }

}
