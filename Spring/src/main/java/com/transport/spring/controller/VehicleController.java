package com.transport.spring.controller;

import com.transport.spring.entity.FirmEntity;
import com.transport.spring.entity.VehicleEntity;
import com.transport.spring.repository.FirmRepository;
import com.transport.spring.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class VehicleController {


    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    FirmRepository firmRepository;

    @PostMapping("/api/addVehicle")
    public ResponseEntity<VehicleEntity> addVehicle(@RequestBody VehicleEntity vehicle) {
        if (vehicle.getFirm() != null) {
            FirmEntity wholeFirm = this.firmRepository.findFirmEntityByNip(vehicle.getFirm().getNip());
            vehicle.setFirm(wholeFirm);
        }
        else
            return null;

        try {
            vehicleRepository.save(vehicle);
        } catch (Exception e){
            System.out.println("ERROR vehicle add: " + e.getMessage());
            return null;
        }
        return ResponseEntity.ok(vehicle);
    }

    @RequestMapping("/api/getAllVehicles")
    public List<VehicleEntity> getAllVehicles() {
        return vehicleRepository.findAll();
    }

    @RequestMapping("/api/getAllVehiclesByFirmNip/{nip}")
    public List<VehicleEntity> getAllVehiclesByFirmNip(@PathVariable String nip) {

        return vehicleRepository.findVehicleEntitiesByFirmNip(nip);
    }

    @RequestMapping("/api/getVehicleByVin/{vin}")
    public VehicleEntity getVehicleByVin(@PathVariable String vin){
        return vehicleRepository.findVehicleEntityByVin(vin);
    }

    @DeleteMapping("/api/deleteVehicle/{vin}")
    public void deleteVehicle(@PathVariable String vin) {
        VehicleEntity a = vehicleRepository.findVehicleEntityByVin(vin);
        try {
            if ( a != null)
                vehicleRepository.delete(a);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
