package com.transport.spring.enums;

/**
 * Interfejs dla enumów z opisem
 */
public interface Described {

    String getHumanReadableDescription();
}
