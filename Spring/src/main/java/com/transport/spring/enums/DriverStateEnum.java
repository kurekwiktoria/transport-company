package com.transport.spring.enums;

/**
 * Enum dla stanu Kierowcy
 */
public enum DriverStateEnum implements Described {
    ACTIVE("Aktywny"),
    ON_VACATION("Na urlopie"),
    FIRED("Zwolniony"),
    SUSPENDED("Zawieszony"),
    UNEMPLOYED("Bezrobotny");

    private String description;

    DriverStateEnum(String description) {
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
