package com.transport.spring.enums;

/**
 * Enum dla typów użytkowników
 */
public enum UserTypeEnum implements Described {
    DRIVER("Kierowca"),
    FORWARDER("Spedytor"),
    ADMIN("Administrator"),
    CLIENT("Zleceniodawca");

    private String description;

    UserTypeEnum(String description) {
        this.description = description;
    }
    @Override
    public String getHumanReadableDescription() {
        return description;
    }
}
