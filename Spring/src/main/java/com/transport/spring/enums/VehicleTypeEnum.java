package com.transport.spring.enums;

/**
 * Enum dla typów pojazdów
 */
public enum VehicleTypeEnum implements Described {
    TRUCK("Ciągnik"),
    STANDARD_TAUTLINER("Naczepa - plandeka"),
    REFRIGERATED_TRAILER("Naczepa - chłodnia"),
    MEGA_TRAILER("Naczepa - plandeka typu MEGA");

    private String description;

    VehicleTypeEnum(String description) {
        this.description = description;
    }
    @Override
    public String getHumanReadableDescription() {
        return description;
    }
}
