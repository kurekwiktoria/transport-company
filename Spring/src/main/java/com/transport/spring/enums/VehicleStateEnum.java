package com.transport.spring.enums;

/**
 * Enum dla typów pojazdów
 */
public enum VehicleStateEnum implements Described {
    AVAILABLE("Dostępny"),
    ON_THE_ROAD("W trasie"),
    IN_REPAIR("W naprawie"),
    UNAVAILABLE("Niedostępny");

    private String description;

    VehicleStateEnum(String description) {
        this.description = description;
    }
    @Override
    public String getHumanReadableDescription() {
        return description;
    }
}
