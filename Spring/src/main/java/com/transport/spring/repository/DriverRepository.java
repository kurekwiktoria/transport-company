package com.transport.spring.repository;

import com.transport.spring.entity.DriverEntity;
import com.transport.spring.entity.UserEntity;
import com.transport.spring.enums.DriverStateEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface DriverRepository extends CrudRepository<DriverEntity, Integer> {

    @Override
    List<DriverEntity> findAll();

    /**
     * Znajdź kierowcę po peselu
     * @param pesel
     * @return
     */
    DriverEntity findDriverEntityByPesel(String pesel);

    /**
     * Znajdż wszystkich kierowców, którzy nie są zatrudnieni
     * @return
     */
    List<DriverEntity> findDriverEntitiesByFirmIsNull();

    /**
     * Znajdź wszystkich kierowców, którzy są zatrudnieni do danej firmy
     * @param firmNip
     * @return
     */
    List<DriverEntity> findDriverEntitiesByFirmNip(String firmNip);

    /**
     * Wyszukuje kierowców o danym stanie
     * @param driverStateEnum
     * @return
     */
    List<DriverEntity> findDriverEntitiesByState(DriverStateEnum driverStateEnum);

    /**
     * Znajdź kierowców, którzy mają nieaktualne prawo jazdy
     * @param date
     * @return
     */
    List<DriverEntity> findDriverEntitiesByDrivingLicenceExpiryDateLessThanEqual(Date date);

    /**
     * Znajdź kierowców, którzy mają nieaktualne badania
     * @param date
     * @return
     */
    List<DriverEntity> findDriverEntitiesByMedicalExaminationExpiryDateLessThanEqual(Date date);
}