package com.transport.spring.repository;

import com.transport.spring.entity.DriverEntity;
import com.transport.spring.entity.FirmEntity;
import com.transport.spring.enums.DriverStateEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface FirmRepository extends CrudRepository<FirmEntity, Integer> {

    @Override
    List<FirmEntity> findAll();

    /**
     * Znajdź firmę po nipie
     *
     * @param nip
     * @return
     */
    FirmEntity findFirmEntityByNip(String nip);

    /**
     * Znajdź firmę po nazwie
     */

    FirmEntity findFirmEntityByName(String name);
}