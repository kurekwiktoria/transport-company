package com.transport.spring.repository;

import com.transport.spring.entity.VacationEntity;
import com.transport.spring.entity.VehicleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface VacationRepository extends CrudRepository<VacationEntity, Integer> {

    @Override
    List<VacationEntity> findAll();

    /**
     * Znajdź urlopy kierowców z firmy
     * @param nip
     * @return
     */
    List<VacationEntity> findVacationEntitiesByDriverFirmNip(String nip);

}