package com.transport.spring.repository;

import com.transport.spring.entity.TransportEntity;
import com.transport.spring.entity.VehicleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransportRepository extends CrudRepository<TransportEntity, Integer> {

    @Override
    List<TransportEntity> findAll();

    /**
     * Zwraca transporty, które nie są zakończone
     * @return
     */
    List<TransportEntity> findTransportEntitiesByIsFinishedFalse();

    /**
     * Zwraca transporty, które są zakończone
     * @return
     */
    List<TransportEntity> findTransportEntitiesByIsFinishedTrue();

    /**
     * Zwraca transporty danej firmy
     * @param nip
     * @return
     */
    List<TransportEntity> findTransportEntitiesByVehicleFirmNip(String nip);

    /**
     * Zwraca transporty danego kierowcy
     * @param pesel
     * @return
     */
    List<TransportEntity> findTransportEntitiesByDriverPesel(String pesel);
}