package com.transport.spring.repository;

import com.transport.spring.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    @Override
    List<UserEntity> findAll();

    Page<UserEntity> findAll(Pageable pageable);

    /**
     * Znajdź użytkownika po PESELu
     * @param pesel
     * @return
     */
    UserEntity findUserEntityByPesel(String pesel);

    void deleteDrByPesel(String pesel);

    void deleteForwarderEntityByPesel(String pesel);

    UserEntity findByLogin(String login);

}