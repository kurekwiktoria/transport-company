package com.transport.spring.repository;

import com.transport.spring.entity.RepairEntity;
import com.transport.spring.entity.VehicleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface RepairRepository extends CrudRepository<RepairEntity, Integer> {

    @Override
    List<RepairEntity> findAll();

    /**
     * Znajduje naprawy dla danego pojazdu
     * @param vin
     * @return
     */
    List<RepairEntity> findRepairEntitiesByVehicleVin(String vin);

    /**
     * Znajduje naprawę po id
     */
    RepairEntity findRepairEntityById(Long id);
}