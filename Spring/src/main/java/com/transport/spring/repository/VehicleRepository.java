package com.transport.spring.repository;

import com.transport.spring.entity.VehicleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VehicleRepository extends CrudRepository<VehicleEntity, Integer> {

    @Override
    List<VehicleEntity> findAll();

    /**
     * Znajdź pojazdy należące do danej firmy
     * @param firmNip
     * @return
     */
    List<VehicleEntity> findVehicleEntitiesByFirmNip(String firmNip);

    /**
     * Znajduje pojazd po numerze vin
     * @param vin
     * @return
     */
    VehicleEntity findVehicleEntityByVin(String vin);

    /**
     * usuwa pojazd po numerze vin
     * @param vin
     */
    void deleteVehicleEntityByVin(String vin);
}