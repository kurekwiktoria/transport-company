package com.transport.spring.repository;

import com.transport.spring.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends CrudRepository<OrderEntity, Integer> {

    @Override
    List<OrderEntity> findAll();

    /**
     * Znajdź zlecenie po id
     * @param id
     * @return
     */
    OrderEntity findOrderEntityById(Long id);

    /**
     * Znajdź dostępne zlecenia
     * @return
     */
    List<OrderEntity> findOrderEntitiesByIsAcceptedIsFalse();
    Page<OrderEntity> findOrderEntitiesByIsAcceptedIsFalse(Pageable page);

    /**
     * Znajdź zlecenia dodane przez użytkownika
     * @param pesel
     * @return
     */
    List<OrderEntity> findOrderEntitiesByUserPesel(String pesel);

    /**
     * Znajduje aktywne zlecenia które są możliwe do akceptacji
     * @param date
     * @return
     */
    List<OrderEntity> findOrderEntitiesByIsAcceptedFalseAndLoadingDateIsGreaterThanEqualOrderByLoadingDate(Date date);

    List<OrderEntity> findOrderEntitiesByIsAcceptedTrueAndLoadingDateIsGreaterThanEqualOrderByLoadingDate(Date date);
}